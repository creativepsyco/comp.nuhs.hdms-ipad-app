//
//  Constants.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 This class serves as the repository for all the API interface URLs
 
 APIBaseURL controls the base mapped URL 
 
 It makes of tokens at different places and asummes that the user is authenticated
 
 */
@interface Constants : NSObject
extern NSString * const APIBaseURL;
extern NSString * const UserLoginURL;
extern NSString * const UserPasswordChangeURL;
extern NSString * const UserInfoChangeURL;
extern NSString * const UserInfoGetURL;

+ (NSString*) ApplicationTitleString;

+ (NSString*) PatientAddURL;
+ (NSString*) GetPatientListByURLKeyword: (NSString*) keyword;
+ (NSString*) GetPatientByPatientId: (NSString*) ID;
+ (NSString*) GetPatientByName: (NSString*) aPatientName;
+ (NSString*) ModifyPatientDetailsByID;
+ (NSString*) UserAddURL;
+ (NSString*) GetUserInfo;
+ (NSString*) UpdateUserInfo;
+ (NSString*) ChangePassword;

+ (NSString*) FormAddURL;
+ (NSString*) GetFormsByPatientIDURL: (NSString*) patientID; 
+ (NSString*) GetFormsByFormIDURL: (NSString*) FormID;
+ (NSString*) GetAllFormsURL;
+ (NSString*) DeleteFormByIDURL: (NSString*) FormID;
+ (NSString*) GetDiseaseNameById: (int) Id;

// Breast Pre
+ (NSString*) BreastPreAddURL;
+ (NSString*) BreastPreGetURLByFormID: (NSString*) FormID;
+ (NSString*) BreastPreUpdateURL;

//Breast Intra
+ (NSString*) BreastIntraAddURL;
+ (NSString*) BreastIntraGetURLByFormID: (NSString*) formID;
+ (NSString*) BreastIntraUpdateURL;

//Modification Access
+ (NSString*) RequestModificationAccessURL;
+ (NSString*) RequestModificationAccessGetURL;
+ (NSString*) RequestModificationAccessGrantURL;
@end

//
//  main.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HDMSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HDMSAppDelegate class]));
    }
}

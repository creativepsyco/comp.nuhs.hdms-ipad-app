//
//  HomeViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface HomeViewController ()
{
    NSString *loginState;
}
@end

@implementation HomeViewController

- (id) init
{
    self = [super init];
    if(self) {
        // Custom initialization
        NSLog(@"Doing init");
        //_homeView = [[HomeView alloc]init];
    }
    loginState = @"LOGGEDOUT";
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"Doing init");
        //_homeView = [[HomeView alloc]init];
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![loginState isEqualToString:@"LOGGEDIN"]){
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        vc.view.layer.cornerRadius = 8;
    
    // if([loginState isEqualToString:@"LOGGEDOUT"]){
    [self presentViewController:vc animated:YES completion:^{
        NSLog(@"Logged in Screen Displayed");
        loginState = @"LOGGEDIN";
    }];
    }
}

- (void) viewDidLoad
{
    NSLog(@"view has loaded");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end

//
//  HomeView.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeView.h"

@implementation HomeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSLog(@"Home View instantiated");
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
 CGContextRef context = UIGraphicsGetCurrentContext();
 CGPoint upperLeft = CGPointMake(0.0,0.0);
 CGRect teamFrame = rect;
 if(CGRectContainsPoint(teamFrame,upperLeft)) {
 CGContextSetRGBFillColor(context, 1.0,0.0,0.0,1.0);
 } else {
 CGContextSetRGBFillColor(context, 0.0,0.0,1.0,1.0);
 }
 CGContextFillRect(context, teamFrame);
}


@end

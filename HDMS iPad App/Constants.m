//
//  Constants.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Constants.h"
#import "HDMSModelStorage.h"

@implementation Constants

NSString * const APIBaseURL = @"http://mmedwebdemo.ddns.comp.nus.edu.sg:8080/comp.nuhs.jaxb/api";
//NSString * const APIBaseURL = @"http://localhost:8080/comp.nuhs.jaxb/api";
NSString * const UserLoginURL = @"/usr/login";
NSString * const UserPasswordChangeURL = @"/usr/changepassword";
NSString * const UserInfoChangeURL = @"/usr/updateuserinfo";
NSString * const UserInfoGetURL = @"/usr/getuserinfo";
+ (NSString*) ApplicationTitleString {
    return @"HDMS System";
}

+ (NSString*) PatientAddURL 
{
    NSString* access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/patientmng/addpatient?token=%@", access_token];
}

+ (NSString*) GetPatientListByURLKeyword:(NSString *)keyword
{
    NSString* access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    NSString* escapedUrlString = [keyword stringByAddingPercentEscapesUsingEncoding:
     NSASCIIStringEncoding];
    return [NSString stringWithFormat:@"/patientmng/getpatient?page=1&token=%@&keyword=%@", access_token, escapedUrlString];
}

+ (NSString*) GetPatientByPatientId:(NSString *)ID
{
    NSString* access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/patientmng/getpatientbyid?page=1&token=%@&patientid=%@", access_token, ID];
}

+ (NSString*) GetPatientByName:(NSString *)aPatientName
{
    NSString* access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/patientmng/getpatientbyname?page=1&token=%@&patientname=%@", access_token, aPatientName];    
}

+ (NSString*) ModifyPatientDetailsByID
{
    NSString *access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/patientmng/updatepatientbyid?token=%@&", access_token]; 
}

+ (NSString*) UserAddURL
{
    return [NSString stringWithFormat:@"/usr/register"]; 
}

+ (NSString*) GetUserInfo
{
    NSString *userid = [[HDMSModelStorage storageObject] UserObject].userid;
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/usr/getuserinfo?token=%@&userid=%@", token, userid];
}

+ (NSString*) UpdateUserInfo
{
    return [NSString stringWithFormat:@"/usr/updateuserinfo"]; 
}

+ (NSString*) ChangePassword
{
    return [NSString stringWithFormat:@"/usr/changepassword"]; 
}

+(NSString*) FormAddURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/form/addform?token=%@", token];
}

+(NSString*) GetDiseaseNameById:(int) Id
{
    switch ((int)Id) {
        case 0:
            return @"CLEFT";
            break;
        case 1:
            return @"BREASTRECON";
        default:
            break;
    }
    return @"NULL";
}

+(NSString*) GetFormsByPatientIDURL:(NSString *)patientID
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/form/getformbypatient?token=%@&patientid=%@", token, patientID];
}

+(NSString*) GetFormsByFormIDURL:(NSString *)FormID
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/form/getformbyid?token=%@&formid=%@", token, FormID];
}

+(NSString*) GetAllFormsURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/form/getallforms?token=%@", token];
}

+(NSString*) DeleteFormByIDURL:(NSString *)FormID
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/form/deleteformbyid?token=%@&formid=%@", token, FormID];
}

+(NSString*) BreastPreAddURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastpre/addbreastpre?token=%@", token];
}

+(NSString*) BreastPreGetURLByFormID:(NSString *)FormID
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastpre/getbreastprebyid?token=%@&formid=%@", token, FormID];
}

+(NSString*) BreastPreUpdateURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastpre/updatebreastprebyid?token=%@", token];
}

+(NSString*) BreastIntraAddURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastintra/addbreastintra?token=%@", token];
}
+(NSString*) BreastIntraGetURLByFormID: (NSString*) formID;
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastintra/getbreastintra?token=%@&formid=%@", token, formID];
}
+(NSString*) BreastIntraUpdateURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/breastintra/updatebreastintra?token=%@", token];
}

+(NSString*) RequestModificationAccessURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/otmar/requestmodificationaccess?token=%@", token];
}
+(NSString*) RequestModificationAccessGetURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;
    return [NSString stringWithFormat:@"/otmar/listmodificationaccess?token=%@", token];
}

+(NSString*) RequestModificationAccessGrantURL
{
    NSString *token = [[HDMSModelStorage storageObject] UserObject].access_token;    
    return [NSString stringWithFormat: @"/otmar/grantmodificationaccess?token=%@", token];
}
@end

//
//  HDMSAppConfig.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HDMSAppConfig.h"

@implementation HDMSAppConfig

@synthesize environment = _environment;

- (id)init
{
    
    self = [super init];
    if (self) {
        _environment = TEST;
    }
    return self;
}

@end

//
//  HDMSAppConfig.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// To test individual configuration

typedef enum{
    PRODUCTION = 0,
    TEST,
    DEVELOPMENT,
    DEMO
} APPLICATION_ENV;

@interface HDMSAppConfig : NSObject

@property (nonatomic, assign) APPLICATION_ENV environment;

@end

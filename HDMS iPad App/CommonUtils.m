//
//  CommonUtils.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CommonUtils.h"
#import "Constants.h"
#import "HDMSModelStorage.h"

@implementation CommonUtils
+ (void)DisplayModalPopup
{
	UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
                          @"The username or password entered is wrong"
                                                  message			:
                          @"Try it Again "
                          delegate
                                                            :self cancelButtonTitle:
                          @"Close"
                                           otherButtonTitles:nil];
    
	[alert show];
}

+ (void) DisplayModalPopupWithMessage: (NSString*) message
{
    UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
                        [Constants ApplicationTitleString]
                                                  message			: message                          delegate
                                                            :self cancelButtonTitle:
                          @"Close"
                                           otherButtonTitles:nil];
    
	[alert show];
}

+(void) StandardDebugLogWithString:(NSString *)theString
{
    NSLog(@"%@", theString);
}

+(void) SwitchOnRestKitDebug
{
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
}

+ (NSDictionary*) CalculateDifferenceOf: (NSDictionary*) Dict1 With: (NSDictionary*) Dict2
{
    NSMutableDictionary* mydictOld = [[NSMutableDictionary alloc]init];
    NSMutableDictionary* mydictNew = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *DictDiff = [[NSMutableDictionary alloc]init];
    
    for (NSString* key1 in Dict1) {
        NSString* value1 = [Dict1 objectForKey:key1];
        NSString* value2 = [Dict2 objectForKey:key1];
        if (![value1 isEqualToString:value2]){
            [mydictOld setObject:value1 forKey:key1];
            [mydictNew setObject:value2 forKey:key1];
        }
    }
    [mydictOld setValue:[[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"] forKey:@"formID"];
    [mydictNew setValue:[[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"] forKey:@"formID"];
    [DictDiff setValue:[mydictOld copy] forKey:@"old"];
    [DictDiff setValue:[mydictNew copy] forKey:@"new"];
    return [DictDiff copy];
}
@end

//
//  CommonUtils.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Restkit/RestKit.h"

/*!
 *   Permissions & AccessLevels Enum Type
 */
typedef enum {
	IT_STAFF, ADMIN_STAFF, DATA_ENTRY_STAFF
} USER_ACCESS_LEVEL;

typedef enum {
	FORM_MODE_EDIT, FORM_MODE_PARTIAL_EDIT, FORM_MODE_NEW, FORM_MODE_DEFAULT
} FORM_MODE;

@interface CommonUtils : NSObject
+ (void)DisplayModalPopup;
+ (void)DisplayModalPopupWithMessage:(NSString *)message;

// Debugging Methods
+ (void)StandardDebugLogWithString:(NSString *)theString;
+ (void)DebugLog:(NSObject *)string;

/*! Debugging With Restkit
 *    Flick the switch to get debug info
 *    In the event of meeting with some issues
 */
+ (void)SwitchOnRestKitDebug;

+ (NSDictionary*) CalculateDifferenceOf: (NSDictionary*) Dict1 With: (NSDictionary*) Dict2;
@end
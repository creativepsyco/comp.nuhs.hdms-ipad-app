//
//  AddUserViewController.h
//  HDMS iPad App
//
//  Created by Xinyu Li on 27/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface AddUserViewController : UIViewController <RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *UserIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UITextField *EmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ConfirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AccessLevelSegment;

- (IBAction)AddNewUserAction:(id)sender;
- (IBAction)ClearFieldsAction:(id)sender;

@end

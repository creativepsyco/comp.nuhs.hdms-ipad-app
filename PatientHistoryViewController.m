//
//  PatientHistoryViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PatientHistoryViewController.h"
#import "HDMSModelStorage.h"
#import "AddFormViewController.h"
#import "BreastReconViewController.h"

@interface PatientHistoryViewController ()
{
    NSArray *objects;
	Boolean NoResultsToDisplay;
}
@end

@implementation PatientHistoryViewController
@synthesize ResultsTableView;
@synthesize PatientBirthdayLabel;
@synthesize PatientNameLabel;
@synthesize PatientIDLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.ResultsTableView.dataSource	= self;
	self.ResultsTableView.delegate		= self;
	objects				= [[NSArray alloc] init];
	NoResultsToDisplay	= YES;
    
    
    [CommonUtils SwitchOnRestKitDebug];
    //Make a webrequest by PatientID
    NSString *patientID = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"];
    PatientIDLabel.text = patientID;
    PatientNameLabel.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"name"];
    [[RKClient sharedClient] get:[Constants GetFormsByPatientIDURL: patientID] delegate:self];
}

- (void)viewDidUnload
{
    [self setResultsTableView:nil];
    [self setPatientBirthdayLabel:nil];
    [self setPatientNameLabel:nil];
    [self setPatientIDLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)DidSelectTheCorrectForm
{
	NSString *str = [NSString stringWithFormat: @"selected row: %i section: %i", [self.ResultsTableView indexPathForSelectedRow].row, [self.ResultsTableView indexPathForSelectedRow].section];
    [CommonUtils StandardDebugLogWithString:str];
    
	if ([objects count] > 0) {
        //		int selected_row = [self.resultsTableView indexPathForSelectedRow].row;
        [self AddSelectedFormInsideModel];
		return YES;
	}
    
	return NO;
}

-(void) AddSelectedFormInsideModel
{
    int selected_row = [self.ResultsTableView indexPathForSelectedRow].row;
    NSDictionary* FormDict = [objects objectAtIndex:selected_row];
    [HDMSModelStorage storageObject].FormObject = [FormDict copy];
}


- (IBAction)DeleteHistoryRecordAction:(id)sender {
    if ([self DidSelectTheCorrectForm])
    {
        NSString *FormId = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
        
        [[RKClient sharedClient] post:[Constants DeleteFormByIDURL:FormId] params:NULL delegate:self];
    } else {
        [CommonUtils DisplayModalPopupWithMessage:@"Please Select a Correct Form"];
    }
}

- (IBAction)AddNewHistoryRecordAction:(id)sender {
    // Assumption: Patient already exists
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    AddFormViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddFormView"]; 
    NSArray							*mArr	= self.splitViewController.viewControllers;
    NSArray							*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
    
    self.splitViewController.viewControllers = mArr2;

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	if (NoResultsToDisplay) {
		return 1;
	}
    
	return [objects count];
}

/* For setting the individual cell properties in case of a redraw */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell			= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	// Configure the cell...
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    
	if (NoResultsToDisplay && (indexPath.row == 0)) {
		cell.textLabel.text = @"No Results";
	}
    
	if (NoResultsToDisplay == NO) {
		NSDictionary *patientDiseaseObject = [objects objectAtIndex:indexPath.row];
		cell.textLabel.text			= [patientDiseaseObject objectForKey:@"disease"];
		cell.detailTextLabel.text	= [patientDiseaseObject objectForKey:@"admissionDate"];
		cell.imageView.image		= [UIImage imageNamed:@"presence_offline.png"];
		cell.accessoryType			= UITableViewCellAccessoryDetailDisclosureButton;
	}
    
	return cell;
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if ([request isGET] && ([response statusCode] == 201)) {
		NSString *json = [response bodyAsString];
        
		if ((json == nil) || (json == NULL) || [json isEqualToString:@"null"]) {
			NoResultsToDisplay	= YES;
			objects				= nil;
			[ResultsTableView reloadData];
			return;
		}
        
        NoResultsToDisplay = NO;
		id <RKParser>	parser		= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
		NSError			*error		= nil;
		NSDictionary	*results	= [parser objectFromString:json error:&error];
        
		[CommonUtils StandardDebugLogWithString:[NSString stringWithFormat:@"Response %@", [response bodyAsString]]];
		[self showJsonSearchResults:results];
    } else if ([request isPOST])
    {
        if([response statusCode]==201)
        {
            [CommonUtils DisplayModalPopupWithMessage:@"Carried out the post request successfully"];
            NSString *patientID = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"];
            [[RKClient sharedClient] get:[Constants GetFormsByPatientIDURL: patientID] delegate:self];
        } else {
            [CommonUtils DisplayModalPopupWithMessage:@"Was not able to carry out the request"];
        }
    }

}

- (void)showJsonSearchResults:(NSDictionary *)results
{
	NSMutableArray *myObjects = [[NSMutableArray alloc] init];
	// store into the object table
	NSObject *patientsResultObject = [results objectForKey:@"form"];
    
	if ([patientsResultObject respondsToSelector:@selector(objectAtIndex:)]) {
		// Its an array
		NSArray *patientsResult = [results objectForKey:@"form"];
        
		for (NSDictionary *patientRecord in patientsResult) {
			[myObjects addObject:patientRecord];
		}
        
		objects = [NSArray arrayWithArray:[patientsResult copy]];
	} else {
		// Its a single patient disease object
		NSDictionary *patientRecord = [results objectForKey:@"form"];
		objects = [NSArray arrayWithObject:patientRecord];
	}
    
	// objects = [NSArray arrayWithArray: [myObjects copy]] ;
    
	[ResultsTableView reloadData];
	//    [self.resultsTableView indexPathForSelectedRow];
}

- (IBAction)ViewFormHistoryAction:(id)sender {
    if([self DidSelectTheCorrectForm])
    {
        NSDictionary* patientHistoryRecord = [HDMSModelStorage storageObject].FormObject;
        if([[patientHistoryRecord objectForKey:@"disease"] isEqualToString:@"BREASTRECON"]){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BreastRecon" bundle:nil];
            
            BreastReconViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FirstView"];
            self.splitViewController.viewControllers = vc.viewControllers;
        } else if([[patientHistoryRecord objectForKey:@"disease"] isEqualToString:@"CLEFT"]){
            [CommonUtils DisplayModalPopupWithMessage:@"Not Yet Implemented"];
        }
    }
}
@end

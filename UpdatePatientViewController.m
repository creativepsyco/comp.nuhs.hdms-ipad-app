//
//  UpdatePatientViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 20/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UpdatePatientViewController.h"
#import "RestKit/RKRequestSerialization.h"
#import "HDMSModelStorage.h"
#import "Constants.h"


@interface UpdatePatientViewController ()

@end

@implementation UpdatePatientViewController
@synthesize patientMenu;
@synthesize PatientID;
@synthesize Gender;
@synthesize Ethnicity;
@synthesize Weight;
@synthesize Occupation;
@synthesize Address;
@synthesize Name;
@synthesize Birthdate;
@synthesize Height;
@synthesize Smoker;
@synthesize HomeNumber;
@synthesize MobileNumber;
@synthesize OfficeNumber;
@synthesize MotherID;
@synthesize FatherID;
@synthesize patientDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self initializeWithPatient:[HDMSModelStorage storageObject].PatientObject];
    [self initializeWithDefaultPatient];
}

- (void)viewDidUnload
{
    [self setPatientID:nil];
    [self setGender:nil];
    [self setEthnicity:nil];
    [self setWeight:nil];
    [self setOccupation:nil];
    [self setAddress:nil];
    [self setName:nil];
    [self setBirthdate:nil];
    [self setHeight:nil];
    [self setSmoker:nil];
    [self setHomeNumber:nil];
    [self setMobileNumber:nil];
    [self setOfficeNumber:nil];
    [self setMotherID:nil];
    [self setFatherID:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initializeWithDefaultPatient
{
    if([HDMSModelStorage storageObject].PatientObject) {
        PatientID.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"];
         Name.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"name"];
        
        [[RKClient sharedClient] get:[Constants GetPatientByPatientId:[[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"]] delegate:self];
        NSLog(@"%@", [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"]);
    }
}

- (void) initializeWithPatient: (NSDictionary*)  patient
{
//    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
//    NSLog(@"PATIENT  %@", [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"] );
    if([HDMSModelStorage storageObject].PatientObject) {
        PatientID.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"];
        NSLog(@"The Patient ID in effect: %@", PatientID.text);
        NSLog(@"The Patient Dictionary %@", [patientDict objectForKey:@"identification"]);
        Name.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"name"];
        // Make the GET request
         [[RKClient sharedClient] get:[Constants GetPatientByPatientId:[[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"]] delegate:self];
    }
}
- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if ([request isGET]){
        if([response statusCode] == 201){
            //do the settingup 
            NSString * json = [response bodyAsString];
            
            id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
            NSError			*error	= nil;
            NSDictionary * results = [parser objectFromString:json error:&error];
            [self SetPatientInformationIntoFields:results];
        }
        else {
            [self ShowPopupDialogBoxWithMessage:@"cant load the patient, check internet connection or enter the correct patient ID"];
        }
    }
    
    if ([request isPOST]) {
        if([response statusCode] == 201) {
            [self ShowPopupDialogBoxWithMessage:@"Successfully modified Patient details"];
        } else {
            [self ShowPopupDialogBoxWithMessage:[NSString stringWithFormat:@"Could not modify patient info, here is the error %@", [response bodyAsString]] ];
        }
    }
}

-(void) SetPatientInformationIntoFields: (NSDictionary *) PatientInfo
{
    self.PatientID.text = [PatientInfo objectForKey: @"identification"];
    self.Name.text =[PatientInfo objectForKey: @"name"];
    self.Gender.text =[PatientInfo objectForKey: @"gender"];
    self.Birthdate.text =[PatientInfo objectForKey: @"birthday"];
    self.Ethnicity.text =[PatientInfo objectForKey: @"ethnic"];
    self.Height.text=[PatientInfo objectForKey: @"height"];
    self.Weight.text=[PatientInfo objectForKey: @"weight"];
    self.Smoker.text=[PatientInfo objectForKey: @"smoker"];
    self.Occupation.text=[PatientInfo objectForKey: @"occupation"];
    self.Address.text=[PatientInfo objectForKey: @"address"];
    self.HomeNumber.text=[PatientInfo objectForKey: @"homeNumber"];
    self.OfficeNumber.text=[PatientInfo objectForKey: @"officeNumber"];
    self.MobileNumber.text=[PatientInfo objectForKey: @"mobileNumber"];
    self.MotherID.text=[PatientInfo objectForKey: @"motherIdentification"];
    self.FatherID.text=[PatientInfo objectForKey: @"fatherIdentification"];
}

- (NSString *)ApplicationTitle {
    return @"HDMS iPad Application";
}

- (void) ShowPopupDialogBoxWithMessage: (NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self ApplicationTitle] 
                                                    message:message 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)UpdatePatientAction:(id)sender {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.PatientID.text, @"identification", 
                            self.Name.text, @"name",
                            self.Gender.text, @"gender",
                            self.Birthdate.text, @"birthday",
                            self.Ethnicity.text, @"ethnic",
                            self.Height.text, @"height",
                            self.Weight.text, @"weight",
                            self.Smoker.text, @"smoker",
                            self.Occupation.text, @"occupation",
                            self.Address.text , @"address",
                            self.HomeNumber.text, @"homeNumber",
                            self.OfficeNumber.text, @"officeNumber",
                            self.MobileNumber.text, @"mobileNumber",
                            self.MotherID.text, @"motherIdentification",
                            self.FatherID.text, @"fatherIdentification",
                            nil];
    
    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    
    [[RKClient sharedClient] post:[Constants ModifyPatientDetailsByID] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (IBAction)ClearAllFieldsAction:(id)sender {
}
@end

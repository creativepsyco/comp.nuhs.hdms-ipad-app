//
//  HDMSUser.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HDMSUser.h"

@implementation HDMSUser

@synthesize name;
@synthesize userid;
@synthesize loginState;
@synthesize access_token;
@synthesize email;
@synthesize access_level;

@end
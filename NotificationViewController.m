//
//  NotificationViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 12/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NotificationViewController.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"
#import "Constants.h"

@interface NotificationViewController ()
{
    NSArray* objects;
    Boolean NoResultsToDisplay;
    NSDictionary* current_selected_object;
}

@end

@implementation NotificationViewController
@synthesize NotificationTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
	if (self) {
		// Custom initialization
	}
    
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    objects = [[NSArray alloc]init];
    current_selected_object = [[NSDictionary alloc]init];
    self.NotificationTableView.dataSource = self;
    self.NotificationTableView.delegate = self;
    NoResultsToDisplay	= YES;
    [[RKClient sharedClient] get:[Constants RequestModificationAccessGetURL] delegate:self];
}

- (void)viewDidUnload
{
    [self setNotificationTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (NoResultsToDisplay) {
		return 1;
	}
    return [objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell			= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	// Configure the cell...
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    
	if (NoResultsToDisplay && (indexPath.row == 0)) {
		cell.textLabel.text = @"No Notifications";
	}
    
	if (NoResultsToDisplay == NO) {
		NSDictionary *notificationObject = [objects objectAtIndex:indexPath.row];
		cell.textLabel.text			= [notificationObject objectForKey:@"formid"];
		cell.detailTextLabel.text	= [notificationObject objectForKey:@"userid"];
		cell.imageView.image		= [UIImage imageNamed:@"presence_offline.png"];
        if([[notificationObject objectForKey:@"approved"] isEqualToString:@"1"])
        {
            //change color
            cell.backgroundColor = [UIColor greenColor];
        }
	}
    
	return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row_selected = indexPath.row;
    current_selected_object = [objects objectAtIndex:row_selected];
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isGET] && ([response statusCode] == 201)) {
		NSString *json = [response bodyAsString];
        
		if ((json == nil) || (json == NULL) || [json isEqualToString:@"null"]) {
			NoResultsToDisplay	= YES;
			objects				= nil;
			[NotificationTableView reloadData];
			return;
		}
        
		NoResultsToDisplay = NO;
		id <RKParser>	parser		= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
		NSError			*error		= nil;
		NSDictionary	*results	= [parser objectFromString:json error:&error];
        
		NSLog(@"Response %@", [response bodyAsString]);
		[self showJsonSearchResults:results];
	}
    if ([request isPOST]) {
        if ([response statusCode] == 201) {
            [CommonUtils DisplayModalPopupWithMessage:@"Successfully Approved the request"];
        }
        else {
            [CommonUtils DisplayModalPopupWithMessage:@"Failed to approve the request"];
        }
    }
}


- (void)showJsonSearchResults:(NSDictionary *)results
{
	NSMutableArray *myObjects = [[NSMutableArray alloc] init];
	// store into the object table
	NSObject *NotificationResultObject = [results objectForKey:@"otmar"];
    
	if ([NotificationResultObject respondsToSelector:@selector(objectAtIndex:)]) {
		// Its an array
		NSArray *notificationResults = [results objectForKey:@"otmar"];
        
		for (NSDictionary *notificationRecord in notificationResults) {
			[myObjects addObject:[notificationRecord objectForKey:@"requestid"]];
		}
        
		objects = [NSArray arrayWithArray:[notificationResults copy]];
	} else {
		// Its a single notification object
		NSDictionary *notificationRecord = [results objectForKey:@"otmar"];
		objects = [NSArray arrayWithObject:notificationRecord];
	}
    
	// objects = [NSArray arrayWithArray: [myObjects copy]] ;
    
	[NotificationTableView reloadData];
	//    [self.resultsTableView indexPathForSelectedRow];
}

- (BOOL)DidSelectTheCorrectNotificationRecord
{
	NSString *str = [NSString stringWithFormat: @"selected row: %i section: %i", [self.NotificationTableView indexPathForSelectedRow].row, [self.NotificationTableView indexPathForSelectedRow].section];
    [CommonUtils StandardDebugLogWithString:str];
    
	if ([objects count] > 0) {
        //		int selected_row = [self.resultsTableView indexPathForSelectedRow].row;
      //  [self AddSelectedPatientInsideModel];
		return YES;
	}
    
	return NO;
}


- (IBAction)ViewDifference:(UIButton *)sender {
    [HDMSModelStorage storageObject].Diff_Object = [current_selected_object copy];
    [self performSegueWithIdentifier:@"FromNotificationCenterToDiff" sender:self];
}

- (IBAction)ApproveRequest:(UIButton *)sender {
    NSString* current_request_id = [current_selected_object objectForKey:@"requestid"];
    NSDictionary	*params		= [NSDictionary dictionaryWithObjectsAndKeys:
                         current_request_id,@"requestid",
                                   nil];
    // RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    
    [[RKClient sharedClient] post:[Constants RequestModificationAccessGrantURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    
}

- (IBAction)Refresh:(UIButton *)sender {
}
@end

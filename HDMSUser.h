//
//  HDMSUser.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    USER_LOGGED_IN = 0,
    USER_NOT_LOGGED_IN
}LOGIN_STATE;

@interface HDMSUser : NSObject
@property (nonatomic, assign) LOGIN_STATE loginState;
@property (nonatomic, assign) NSString* access_token;
@property (nonatomic, assign) NSString* email;
@property (nonatomic, assign) NSString* name;
@property (nonatomic, assign) NSString* userid;
@property (nonatomic, assign) NSString* access_level;

@end

//
//  BreastReconTableViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 31/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

/*!
 This is class serves as the table view controller for the three phases of Breast Recon 
 
 It performs the task of opening up the GET request with the server to invoke the detail view controller RKRequest Delegates
 */
@interface BreastReconTableViewController : UITableViewController
- (IBAction)goBacktoHome:(id)sender; 

@property (nonatomic, retain) UIViewController* BreastReconIntraRKRequestDelegate;
@end

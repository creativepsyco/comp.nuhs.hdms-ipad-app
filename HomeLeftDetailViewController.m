//
//  HomeLeftDetailViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeLeftDetailViewController.h"
#import "HomeDetail.h"
#import "PatientHome.h"
#import "UserSettingsViewController.h"
#import "FormSelectPatientViewController.h"
#import "AddUserViewController.h"
#import "UpdateUserViewController.h"
#import "ChangePasswordViewController.h"

@interface HomeLeftDetailViewController ()

@end

@implementation HomeLeftDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
	self = [super initWithStyle:style];

	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Uncomment the following line to preserve selection between presentations.
	// self.clearsSelectionOnViewWillAppear = NO;

	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	// self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (IBAction)ShowHomeProfile:(id)sender
{
	UIStoryboard	*storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
	HomeDetail		*vc			= [storyboard instantiateViewControllerWithIdentifier:@"Profile"];
	NSArray			*mArr		= self.splitViewController.viewControllers;
	NSArray			*mArr2		= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];

	self.splitViewController.viewControllers = mArr2;
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

/*
 *   - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 *   {
 * #warning Potentially incomplete method implementation.
 *    // Return the number of sections.
 *    return 0;
 *   }*/

/*
 *   - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 *   {
 * #warning Incomplete method implementation.
 *    // Return the number of rows in the section.
 *    return 0;
 *   }*/

/*
 *   - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    static NSString *CellIdentifier = @"Cell";
 *    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 *     NSLog(@"Selected%@", indexPath.description);
 *    // Configure the cell...
 *
 *    return cell;
 *   }*/

/*
 *   // Override to support conditional editing of the table view.
 *   - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    // Return NO if you do not want the specified item to be editable.
 *    return YES;
 *   }
 */

/*
 *   // Override to support editing the table view.
 *   - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    if (editingStyle == UITableViewCellEditingStyleDelete) {
 *        // Delete the row from the data source
 *        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 *    }
 *    else if (editingStyle == UITableViewCellEditingStyleInsert) {
 *        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 *    }
 *   }
 */

/*
 *   // Override to support rearranging the table view.
 *   - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 *   {
 *   }
 */

/*
 *   // Override to support conditional rearranging of the table view.
 *   - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    // Return NO if you do not want the item to be re-orderable.
 *    return YES;
 *   }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Navigation logic may go here. Create and push another view controller.
	int rowSelected = indexPath.row;
    int sectionSelected = indexPath.section;
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    if(sectionSelected == 0)
    {
        // patient stuff
        switch (rowSelected) {
            case 0:
            {
                // For Operations
				FormSelectPatientViewController	*vc		= [storyboard instantiateViewControllerWithIdentifier:@"FormSelectPatient"];
				NSArray						*mArr	= self.splitViewController.viewControllers;
				NSArray						*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
                
				self.splitViewController.viewControllers = mArr2;
			}
                break;
            case 1:
            {
                //notification
            }
            default:
                break;
        }
        
    } 
    else if (sectionSelected == 1)
    {
        switch (rowSelected) {
            case 0:
            {
                UserSettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Settings"];
                NSArray		*mArr	= self.splitViewController.viewControllers;
                NSArray		*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
                
                self.splitViewController.viewControllers = mArr2;

            }
                break;
            case 1: 
            {
                AddUserViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"UpdateUserView"];
                NSArray		*mArr	= self.splitViewController.viewControllers;
                NSArray		*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
                
                self.splitViewController.viewControllers = mArr2;
            }
                break;
            case 2:
            {
                ChangePasswordViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordView"];
                NSArray		*mArr	= self.splitViewController.viewControllers;
                NSArray		*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
                
                self.splitViewController.viewControllers = mArr2;

            } break;
            default:
                break;
        }
        
    } else if (sectionSelected == 2)
    {
        switch (rowSelected) {
            case 0:
                break;
            case 1: 
            {
                AddUserViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddUserView"];
                NSArray		*mArr	= self.splitViewController.viewControllers;
                NSArray		*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
                
                self.splitViewController.viewControllers = mArr2;

            } break;
            default:
                break;
        }
    }
	// UIViewController* myCOS = [[self.splitViewController.viewControllers lastObject] topViewController];
	//    myCOS = [storyboard instantiateViewControllerWithIdentifier:@"Settings"];

	// myCOS.view = vc.view;

	// Get the object array

	// [myCOS.navigationController popToRootViewControllerAnimated:YES];

	//  [myCOS.navigationController pushViewController:vc animated:YES];
	// [self.navigationController  pushViewController:vc animated:YES];
}

@end
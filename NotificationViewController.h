//
//  NotificationViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 12/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface NotificationViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,  RKRequestDelegate>

@property (weak, nonatomic) IBOutlet UITableView *NotificationTableView;
- (IBAction)ViewDifference:(UIButton *)sender;
- (IBAction)ApproveRequest:(UIButton *)sender;
- (IBAction)Refresh:(UIButton *)sender;

@end

//
//  BreastIntraEditTab2ViewController.m
//  HDMS iPad App
//
//  Created by Xinyu Li on 4/4/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import "BreastIntraEditTab2ViewController.h"
#import "HDMSModelStorage.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "Constants.h"

@interface BreastIntraEditTab2ViewController ()

@end

@implementation BreastIntraEditTab2ViewController

@synthesize BothChoiceSegementedControl;
@synthesize BothPedicledChoiceSegementedControl;
@synthesize BothPedicledImplantSizeTextField;
@synthesize BothFreeFlapChoiceSegementedControl;
@synthesize BothFreeFlapImplantSizeTextField;
@synthesize AutologousReconChoiceSegementedControl;
@synthesize AutologousReconPedicledChoiceSegmentedControl;
@synthesize AutologousReconFreeFlapSegementedControl;
@synthesize AutologousReconIschemicTimeTextField;
@synthesize AutologousReconDIEPPerforatorsTextFiled;
@synthesize AutologousReconRecepientVesselsSegementedControl;
@synthesize ImplantBasedChoiceSegementedControl;
@synthesize ImplantBasedImplantSizeRTextFiled;
@synthesize ImplantedBasedImplantSizedRTextField;
@synthesize ImplantBasedExpanderSize;
@synthesize ImplantBasedView;
@synthesize AutologousReconstructionView;
@synthesize BothView;
@synthesize BreastReconProcedureSegmentatedControl;
@synthesize SubViewSegmentedControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initialSubViews
{
    self.ImplantBasedView.hidden = NO;
    self.AutologousReconstructionView.hidden = YES;
    self.BothView.hidden = YES;
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    
}

-(void) EnablePartialControls
{
    
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
       // More Controls
}

-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
    
    NSString* breastRecProcedure = [mydir objectForKey:@"breastRecProcedure"];
    if([breastRecProcedure isEqualToString:@"0"])
        [BreastReconProcedureSegmentatedControl setSelectedSegmentIndex:0];
    else if ([breastRecProcedure isEqualToString:@"1"])
        [BreastReconProcedureSegmentatedControl setSelectedSegmentIndex:1];

    NSString* implantBased = [mydir objectForKey:@"implantBased"];
    if([implantBased isEqualToString:@"1"])
        [SubViewSegmentedControl setSelectedSegmentIndex:0];
    NSString* autologousRec = [mydir objectForKey:@"autologousRec"];
    if ([autologousRec isEqualToString:@"1"])
        [SubViewSegmentedControl setSelectedSegmentIndex:1];
    NSString* both = [mydir objectForKey:@"both"];
    if ([both isEqualToString:@"1"])
        [SubViewSegmentedControl setSelectedSegmentIndex:2];
    
    NSString* implantBasedChoice = [mydir objectForKey:@"implantBasedChoice"];
    [ImplantBasedChoiceSegementedControl setSelectedSegmentIndex:([implantBasedChoice integerValue]-1)];
    
    NSString* implantRightSize = [mydir objectForKey:@"implantRightSize"];
    [ImplantBasedImplantSizeRTextFiled setText:implantRightSize];
    
    NSString* implantLeftSize = [mydir objectForKey:@"implantLeftSize"];
    [ImplantedBasedImplantSizedRTextField setText:implantLeftSize];
    
    NSString* expanderSize = [mydir objectForKey:@"expanderSize"];
    [ImplantBasedExpanderSize setText:expanderSize];
    
    NSString* autologousRecChoice = [mydir objectForKey:@"autologousRecChoice"];
    [AutologousReconChoiceSegementedControl setSelectedSegmentIndex:[autologousRecChoice integerValue]];
    
    NSString* pedicledTRAM = [mydir objectForKey:@"pedicledTRAM"];
    if([pedicledTRAM isEqualToString:@"1"])
        [AutologousReconPedicledChoiceSegmentedControl setSelectedSegmentIndex:1];
    
    NSString* pedicledLD = [mydir objectForKey:@"pedicledLD"];
    if([pedicledLD isEqualToString:@"1"])
        [AutologousReconPedicledChoiceSegmentedControl setSelectedSegmentIndex:2];
    
    NSString* freeFlapTRAM = [mydir objectForKey:@"freeFlapTRAM"];
    NSString* freeFlapDIEP = [mydir objectForKey:@"freeFlapDIEP"];
    NSString* freeFlapALT =  [mydir objectForKey:@"freeFlapALT"];
    
    if([freeFlapDIEP isEqualToString:@"1"])
        [AutologousReconFreeFlapSegementedControl setSelectedSegmentIndex:1];
    else if ([freeFlapTRAM isEqualToString:@"1"])
        [AutologousReconFreeFlapSegementedControl setSelectedSegmentIndex:3];
    else [AutologousReconFreeFlapSegementedControl setSelectedSegmentIndex:4];
    
    NSString* lschemicTime = [mydir objectForKey:@"lschemicTime"];
    [AutologousReconIschemicTimeTextField setText:lschemicTime];
    NSString* ofPerforatorInDIEP = [mydir objectForKey:@"ofPerforatorInDIEP"];
    [AutologousReconDIEPPerforatorsTextFiled setText:ofPerforatorInDIEP];
    
    NSString* recepientVessel = [mydir objectForKey:@"recepientVessel"];
    [AutologousReconRecepientVesselsSegementedControl setSelectedSegmentIndex:([recepientVessel integerValue]-1)];
    
    NSString* bothChoice =[mydir objectForKey:@"bothChoice"];
    [BothChoiceSegementedControl setSelectedSegmentIndex:[bothChoice integerValue]];
    NSString* pedicledLDImplantSize = [mydir objectForKey:@"pedicledLDImplantSize"];
    [BothPedicledImplantSizeTextField setText:pedicledLDImplantSize];
    NSString* pedicledTRAMImplantSize = [mydir objectForKey:@"pedicledTRAMImplantSize"];
    [BothPedicledImplantSizeTextField setText:pedicledTRAMImplantSize];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initialSubViews];
    // Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
}

- (void)viewDidUnload
{
    [self setImplantBasedView:nil];
    [self setAutologousReconstructionView:nil];
    [self setBothView:nil];
    [self setBreastReconProcedureSegmentatedControl:nil];
    [self setSubViewSegmentedControl:nil];
    [self setImplantBasedChoiceSegementedControl:nil];
    [self setImplantBasedImplantSizeRTextFiled:nil];
    [self setImplantedBasedImplantSizedRTextField:nil];
    [self setAutologousReconChoiceSegementedControl:nil];
    [self setAutologousReconPedicledChoiceSegmentedControl:nil];
    [self setAutologousReconFreeFlapSegementedControl:nil];
    [self setAutologousReconIschemicTimeTextField:nil];
    [self setAutologousReconDIEPPerforatorsTextFiled:nil];
    [self setAutologousReconRecepientVesselsSegementedControl:nil];
    [self setBothChoiceSegementedControl:nil];
    [self setBothPedicledChoiceSegementedControl:nil];
    [self setBothPedicledImplantSizeTextField:nil];
    [self setBothFreeFlapChoiceSegementedControl:nil];
    [self setBothFreeFlapImplantSizeTextField:nil];
    [self setImplantBasedExpanderSize:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    /*return (interfaceOrientation == UIInterfaceOrientationPortrait);*/
    return YES;
}

- (IBAction)AutologousReconIschemicTimeAction:(id)sender {
}
- (IBAction)SubViewSegmentedControlValueChangedAction:(id)sender {
    switch (SubViewSegmentedControl.selectedSegmentIndex) {
        case 0:
            self.ImplantBasedView.hidden = NO;
            self.AutologousReconstructionView.hidden = YES;
            self.BothView.hidden = YES;
            break;
        case 1:
            self.ImplantBasedView.hidden = YES;
            self.AutologousReconstructionView.hidden = NO;
            self.BothView.hidden = YES;
            break;
        case 2:
            self.ImplantBasedView.hidden = YES;
            self.AutologousReconstructionView.hidden = YES;
            self.BothView.hidden = NO;
            break;
        default:
            break;
    }
}

-(NSString*) sanitizeValue: (NSString*) value
{
    if (value != nil) { 
        return value;
    }
    else {
        return @"";
    }
}

-(void) SanitizeAndStoreValue: (NSString*)value forKey: (NSString*) key
{
    // Only dealing with Breast Intra Object
    [[HDMSModelStorage storageObject].Breast_IntraObject setObject:[self sanitizeValue: value] forKey: key ];
}

-(void) saveValues
{
    NSString* breastRecProcedure = [NSString stringWithFormat: @"%i",BreastReconProcedureSegmentatedControl.selectedSegmentIndex];
    NSString* implantBased = @"0";
    NSString* both = @"0";
    NSString* autologousRec = @"0";
    if(SubViewSegmentedControl.selectedSegmentIndex == 0){
        implantBased = @"1";
    } else if (SubViewSegmentedControl.selectedSegmentIndex == 1) {
        autologousRec = @"1";
    } else {
        both = @"1";
    }
        
    NSString* implantBasedChoice = [NSString stringWithFormat:@"%i", (ImplantBasedChoiceSegementedControl.selectedSegmentIndex + 1) ];
    NSString* implantRightSize = [NSString stringWithFormat:@"%i", ImplantBasedImplantSizeRTextFiled.text];
    NSString* implantLeftSize = [NSString stringWithFormat:@"%i", ImplantedBasedImplantSizedRTextField.text];
    NSString* expanderSize = ImplantBasedExpanderSize.text;
    
    NSString* autologousRecChoice = [NSString stringWithFormat:@"%i", (AutologousReconChoiceSegementedControl.selectedSegmentIndex )];
    NSString* pedicledTRAM = @"0";
    if(AutologousReconPedicledChoiceSegmentedControl.selectedSegmentIndex == 0 || AutologousReconPedicledChoiceSegmentedControl.selectedSegmentIndex == 1 ){
        pedicledTRAM = @"1";
    }
    
    NSString* pedicledLD = @"0";
    if(AutologousReconPedicledChoiceSegmentedControl.selectedSegmentIndex == 2 || AutologousReconPedicledChoiceSegmentedControl.selectedSegmentIndex == 3 ){
        pedicledLD = @"1";
    }
    NSString* freeFlapTRAM = @"0";
    NSString* freeFlapDIEP = @"0";
    NSString* freeFlapALT = @"0";
    int selected_index_free_flap = AutologousReconFreeFlapSegementedControl.selectedSegmentIndex;
    if (selected_index_free_flap == 0 || selected_index_free_flap == 1)
    {
        freeFlapDIEP = @"1";
    } else if (selected_index_free_flap == 2 || selected_index_free_flap == 3)
    {
        freeFlapTRAM = @"1";
    } else {
        freeFlapALT = @"1";
    }
    
    NSString* lschemicTime = AutologousReconIschemicTimeTextField.text;
    NSString* ofPerforatorInDIEP = AutologousReconDIEPPerforatorsTextFiled.text;
    NSString* recepientVessel = [NSString stringWithFormat:@"%i", (AutologousReconRecepientVesselsSegementedControl.selectedSegmentIndex + 1)];
    
    // BOTH
    NSString* bothChoice = [NSString stringWithFormat:@"%i", BothChoiceSegementedControl.selectedSegmentIndex];
    NSString* pedicledLDImplantSize = BothPedicledImplantSizeTextField.text;
    NSString* pedicledTRAMImplantSize = BothPedicledImplantSizeTextField.text;
    NSString* freeFlapTRAMImplantSize = BothFreeFlapImplantSizeTextField.text;
    NSString* freeFlapDIEPImplantSize = BothFreeFlapImplantSizeTextField.text;
    
    
    [self SanitizeAndStoreValue:breastRecProcedure forKey:@"breastRecProcedure"];
    [self SanitizeAndStoreValue:implantBased forKey:@"implantBased"];
    [self SanitizeAndStoreValue:both forKey:@"both"];
    [self SanitizeAndStoreValue:autologousRec forKey:@"autologousRec"];
    [self SanitizeAndStoreValue:implantBasedChoice forKey:@"implantBasedChoice"];
    [self SanitizeAndStoreValue:implantRightSize forKey:@"implantRightSize"];
    [self SanitizeAndStoreValue:implantLeftSize forKey:@"implantLeftSize"];
    [self SanitizeAndStoreValue:expanderSize forKey:@"expanderSize"];
    [self SanitizeAndStoreValue:autologousRecChoice forKey:@"autologousRecChoice"];
    [self SanitizeAndStoreValue:pedicledTRAM forKey:@"pedicledTRAM"];
    [self SanitizeAndStoreValue:pedicledLD forKey:@"pedicledLD"];
    [self SanitizeAndStoreValue:freeFlapTRAM forKey:@"freeFlapTRAM"];
    [self SanitizeAndStoreValue:freeFlapDIEP forKey:@"freeFlapDIEP"];
    [self SanitizeAndStoreValue:freeFlapALT forKey:@"freeFlapALT"];
    [self SanitizeAndStoreValue:lschemicTime forKey:@"lschemicTime"];
    [self SanitizeAndStoreValue:ofPerforatorInDIEP forKey:@"ofPerforatorInDIEP"];
    [self SanitizeAndStoreValue:recepientVessel forKey:@"recepientVessel"];
    [self SanitizeAndStoreValue:bothChoice forKey:@"bothChoice"];
    [self SanitizeAndStoreValue:pedicledLDImplantSize forKey:@"pedicledLDImplantSize"];
    [self SanitizeAndStoreValue:pedicledTRAMImplantSize forKey:@"pedicledTRAMImplantSize"];
    [self SanitizeAndStoreValue:freeFlapTRAMImplantSize forKey:@"freeFlapTRAMImplantSize"];
    [self SanitizeAndStoreValue:freeFlapDIEPImplantSize forKey:@"freeFlapDIEPImplantSize"];
}

- (IBAction)GoToNextTab:(UIBarButtonItem *)sender {
    [self saveValues];
    [self performSegueWithIdentifier:@"SecondTabToThirdTab" sender:self];
}
@end

//
//  BreastPreViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastPreViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}

// Format of tags
// XYZ : X refers to the tab view
// Y: refers to 0 or if there are multiple controls in one tab
// Z: refers to the 
//! Assumption: not more than 20 controls in one page

//  Common Actions
- (IBAction)SelectDateAction:(id)sender;
- (IBAction)PostBreastPreData:(UIBarButtonItem *)sender;

// Tab 1 Edit Actions

- (IBAction)SaveTab1Data:(UIBarButtonItem *)sender;
- (IBAction)SelectASAScore:(UIButton*)sender;
- (IBAction)ComorbidConditionChanged:(UISegmentedControl *)sender;

- (IBAction)SaveTab2Data:(UIBarButtonItem *)sender;
- (IBAction)SaveTab3Data:(UIBarButtonItem *)sender;
- (IBAction)SaveTab4Data:(id)sender;

// Tab 4 Actions

- (IBAction)Tab4GradeSelected:(UISegmentedControl *)sender;


//  Common Actions End

// Picker Controls and Arrays

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray* ASAScoresList;
@property (nonatomic, retain) NSArray* ComorbidConditionsList;
@property (nonatomic, retain) UIDatePicker* datePicker;
@property (nonatomic, retain) NSArray* GradesList;

// Tab1 Edit Fields
@property (weak, nonatomic) IBOutlet UITextField *ReconstructionAge;

@property (weak, nonatomic) IBOutlet UILabel *PlannedDischargeDate; //tag: 101

@property (weak, nonatomic) IBOutlet UILabel *ActualDischargeDate;
@property (weak, nonatomic) IBOutlet UITextField *BraSize;
@property (weak, nonatomic) IBOutlet UITextField *BMI;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ComorbidCondition;
@property (weak, nonatomic) IBOutlet UILabel *ComorbidConditionType;
@property (weak, nonatomic) IBOutlet UITextField *ComorbidConditionTypeOthers;
@property (weak, nonatomic) IBOutlet UILabel *ASAScore;



// Tab1 View Fields

// Tab 2 Edit Fields
@property (weak, nonatomic) IBOutlet UILabel *DateOfSurgery;
@property (weak, nonatomic) IBOutlet UITextField *HospitalStayLength;
@property (weak, nonatomic) IBOutlet UILabel *MasectomyDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *MasectomyDatePrevRight;
@property (weak, nonatomic) IBOutlet UILabel *BiopsyDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *BiopsyDatePrevRight;
@property (weak, nonatomic) IBOutlet UILabel *WLEDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *WLEDatePrevRight;

// Tab3 Edit Fields
@property (weak, nonatomic) IBOutlet UISegmentedControl *LumpPresent;
@property (weak, nonatomic) IBOutlet UISegmentedControl *BreastSwelling;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AreaThickeningState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *RednessState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *DimpleBreastState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *EnlargedLymphNodeState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *MasectomyPosition;

// Tab4 Edit Fields
@property (weak, nonatomic) IBOutlet UILabel *IDCGrade;
@property (weak, nonatomic) IBOutlet UILabel *DCISGrade;
@property (weak, nonatomic) IBOutlet UILabel *ExternalDCISGrade;
@property (weak, nonatomic) IBOutlet UILabel *InfiltrativeDuctalCarcinoma;
@property (weak, nonatomic) IBOutlet UILabel *MucinousCarcinoma;
@property (weak, nonatomic) IBOutlet UITextField *MasectomyOtherReason;
@property (weak, nonatomic) IBOutlet UISegmentedControl *FibroadenomataPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *FatNecrosisPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SacromaPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *RecurrentBreastCarcinomaPresence;


// Tab5 Edit Fields

@property (weak, nonatomic) IBOutlet UITextField *MammogramShowed;
@property (weak, nonatomic) IBOutlet UILabel *MammogramDateDone;
@property (weak, nonatomic) IBOutlet UITextField *XRayShowed;
@property (weak, nonatomic) IBOutlet UILabel *XRayDateDone;

@end

//
//  BreastPostViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPostViewController.h"

@interface BreastPostViewController ()

@end

@implementation BreastPostViewController
@synthesize FirstLabel;
@synthesize SecondLabel;
@synthesize ThirdPage;
@synthesize postComplicationsList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) initPickerArrays
{
    self.postComplicationsList = [[NSArray alloc]initWithObjects:@"Bleeding", @"Hypo-perfusion", @"Flap feels cool compared with adjacent skin", @"Swelling of flap with small pits seen around hair follicles" @"Change in flap colour from normal", @"Presence of Redness", @"Presence of oozing of haemoserous from flap edges", @"Presence of oozing of haemopurulent from flap edges", @"DVT", @"Thrombophlebitis",@"Pain",@"Wound dehiscence (breast/abdomen)", @"Cyst formation", @"Fat necrosis", nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.FirstLabel.text = @"This is the first";
    self.SecondLabel.text = @"This is the second";
    self.ThirdPage.text = @"Yo Dude!!";
}

- (void)viewDidUnload
{
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdPage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

//
//  FormSelectPatientViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 21/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormSelectPatientViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *keyword;
@property (weak, nonatomic) IBOutlet UITableView *tableResults;
@property (weak, nonatomic) IBOutlet UITextField *PatientID;
@property (weak, nonatomic) IBOutlet UITextField *DiseaseName;

@property (weak, nonatomic) IBOutlet UITextField *DateOfAdmission;
- (IBAction)CreateFormAction:(id)sender;
- (IBAction)SearchPatientAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableFormResults;
- (IBAction)EditFormAction:(id)sender;
@end

//
//  BreastPreViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"

@interface BreastPreViewController ()
{
    // Tab Numbers
    // 1 for Tab1 => Patient Details
    // 2 for Tab2 => Masectomy Information
    int current_item_change_id; // Describes the tag property of the item being changed
    
    int current_tab_being_changed; // Describes the view number of the tag being changed 
    
    BOOL edit_mode_on; // Tracks if the edit mode is switched on or not
}

@end

@implementation BreastPreViewController
@synthesize MammogramShowed;
@synthesize MammogramDateDone;
@synthesize XRayShowed;
@synthesize XRayDateDone;
@synthesize IDCGrade;
@synthesize DCISGrade;
@synthesize ExternalDCISGrade;
@synthesize InfiltrativeDuctalCarcinoma;
@synthesize MucinousCarcinoma;
@synthesize MasectomyOtherReason;
@synthesize FibroadenomataPresence;
@synthesize FatNecrosisPresence;
@synthesize SacromaPresence;
@synthesize RecurrentBreastCarcinomaPresence;
@synthesize LumpPresent;
@synthesize BreastSwelling;
@synthesize AreaThickeningState;
@synthesize RednessState;
@synthesize DimpleBreastState;
@synthesize EnlargedLymphNodeState;
@synthesize MasectomyPosition;
@synthesize DateOfSurgery;
@synthesize HospitalStayLength;
@synthesize MasectomyDatePrevLeft;
@synthesize MasectomyDatePrevRight;
@synthesize BiopsyDatePrevLeft;
@synthesize BiopsyDatePrevRight;
@synthesize WLEDatePrevLeft;
@synthesize WLEDatePrevRight;
@synthesize ReconstructionAge;
@synthesize PlannedDischargeDate;
@synthesize ActualDischargeDate;
@synthesize BraSize;
@synthesize BMI;
@synthesize ComorbidCondition;
@synthesize ComorbidConditionType;
@synthesize ComorbidConditionTypeOthers;
@synthesize ASAScore;

// Pickers
@synthesize pickerView, datePicker, ASAScoresList, ComorbidConditionsList, GradesList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// Stuff that is loaded just after start comes here

-(void) initPickerArrays
{
    self.ComorbidConditionsList = [[NSArray alloc]initWithObjects:@"COPD", @"DM Type I", @"CHF/CCF", @"CVA", @"IHD", @"Hypertension", @"Others", nil];
    self.ASAScoresList = [[NSArray alloc]initWithObjects:@"ASA I", @"ASA II", @"ASA III", @"ASA IV", @"ASA V", nil];
    self.GradesList = [[NSArray alloc] initWithObjects:@"Grade 1", @"Grade 2", @"Grade 3", nil];
}



- (void)initializeThePickerView {
    // Must display the UIPicker
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    self.pickerView.hidden = YES;
}

- (UIPickerView*) constructPickerViewWithTag: (int) tagNumber
{
    UIPickerView *myPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    myPicker.dataSource = self;
    myPicker.delegate = self;
    myPicker.showsSelectionIndicator = YES;
    myPicker.tag = tagNumber;
    [self.view addSubview:myPicker];
    myPicker.hidden = YES;
    return myPicker;
}

- (void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];      
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(DateChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

// is referred to by DatePicker on completion
- (void)DateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    NSString *myDate = [NSString stringWithFormat:@"%@",
                        [df stringFromDate:datePicker.date]];
    switch (current_item_change_id) {
            // Do stuff relevant to changing date of different labels
        
        case 101:
            //planned discharge date
            PlannedDischargeDate.text = myDate;
            break;
        case 102: // actual 
            ActualDischargeDate.text = myDate;
            break;
            
            // Second Tab
        case 201: DateOfSurgery.text = myDate; break;
        case 202: MasectomyDatePrevLeft.text = myDate; break;        
        case 203: MasectomyDatePrevRight.text = myDate; break;
        case 204: BiopsyDatePrevLeft.text = myDate; break;
        case 205: BiopsyDatePrevRight.text = myDate; break;
        case 206: WLEDatePrevLeft.text = myDate; break;
        case 207: WLEDatePrevRight.text = myDate; break;
            // Tab 5
        case 501: MammogramDateDone.text = myDate; break;
        case 502: XRayDateDone.text = myDate; break;
        default:
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initPickerArrays];
    [self initDateStuff];
}

- (void)viewDidUnload
{
    [self setReconstructionAge:nil];
    [self setPlannedDischargeDate:nil];
    [self setActualDischargeDate:nil];
    [self setBMI:nil];
    [self setBraSize:nil];
    [self setComorbidCondition:nil];
    [self setComorbidConditionType:nil];
    [self setComorbidConditionTypeOthers:nil];
    [self setDateOfSurgery:nil];
    [self setHospitalStayLength:nil];
    [self setMasectomyDatePrevLeft:nil];
    [self setMasectomyDatePrevRight:nil];
    [self setBiopsyDatePrevLeft:nil];
    [self setBiopsyDatePrevRight:nil];
    [self setWLEDatePrevLeft:nil];
    [self setWLEDatePrevRight:nil];
    [self setLumpPresent:nil];
    [self setBreastSwelling:nil];
    [self setAreaThickeningState:nil];
    [self setRednessState:nil];
    [self setDimpleBreastState:nil];
    [self setEnlargedLymphNodeState:nil];
    [self setMasectomyPosition:nil];
    [self setIDCGrade:nil];
    [self setDCISGrade:nil];
    [self setExternalDCISGrade:nil];
    [self setInfiltrativeDuctalCarcinoma:nil];
    [self setMucinousCarcinoma:nil];
    [self setMasectomyOtherReason:nil];
    [self setMammogramShowed:nil];
    [self setMammogramDateDone:nil];
    [self setXRayShowed:nil];
    [self setXRayDateDone:nil];
    [self setFibroadenomataPresence:nil];
    [self setFatNecrosisPresence:nil];
    [self setSacromaPresence:nil];
    [self setRecurrentBreastCarcinomaPresence:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}



#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the stuff relevant to the picker view pressed 
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //For Comorbid
            return [self.ComorbidConditionsList count];
            break;
        case 104: //For ASA Score
            return [self.ASAScoresList count];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList count]; break;
        default:
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            return [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            return [self.ASAScoresList objectAtIndex:row];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    return @"NONE";
} 

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //Comorbid
            self.ComorbidConditionType.text = [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            //ASA
            self.ASAScore.text = [self.ASAScoresList objectAtIndex:row];
            break;
            // Tab 4
        case 401: IDCGrade.text = [self.GradesList objectAtIndex:row]; break;
        case 402: DCISGrade.text = [self.GradesList objectAtIndex:row]; break;        
        case 403: ExternalDCISGrade.text = [self.GradesList objectAtIndex:row]; break;
        case 404: InfiltrativeDuctalCarcinoma.text = [self.GradesList objectAtIndex:row]; break;
        case 405: MucinousCarcinoma.text = [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    thePickerView.hidden = YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)PostBreastPreData:(UIBarButtonItem *)sender {
    [self saveValues];
}


- (IBAction)SelectASAScore:(UIButton*)sender {
    current_item_change_id = sender.tag;
    pickerView = [self constructPickerViewWithTag:current_item_change_id];
    pickerView.hidden = (!pickerView.hidden);
}

- (IBAction)ComorbidConditionChanged:(UISegmentedControl *)sender {
    //Display a UIPicker
    current_item_change_id = sender.tag;
    if(self.ComorbidCondition.selectedSegmentIndex == 0){
        self.pickerView = [self constructPickerViewWithTag:sender.tag];
        self.pickerView.hidden = NO;
    }
}

// Save the data related to the first tab
- (IBAction)SaveTab1Data:(UIBarButtonItem *)sender {
    // Save Data from Tab1 into HDMSOfflineCache
    NSString* formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    
    NSString* reconstructionAge = [self sanitizeValue:ReconstructionAge.text];
    NSString* plannedDischargeDate = [self sanitizeValue:PlannedDischargeDate.text];
    NSString* actualDischargeDate = [self sanitizeValue:ActualDischargeDate.text];
    NSString* bmi = [self sanitizeValue:BMI.text];
    NSString* braSize = [self sanitizeValue:BraSize.text];
    NSString* comorbidCondition = [self sanitizeValue:[NSString stringWithFormat:@"%i", ComorbidCondition.selectedSegmentIndex]];
    NSString* comorbidConditionType = [self sanitizeValue:[NSString stringWithFormat:@"%i", [self.ComorbidConditionsList indexOfObject:self.ComorbidConditionType.text ]+1]];
    NSString* comorbidConditionOthers = [self sanitizeValue:ComorbidConditionTypeOthers.text];
    NSString* asaScore = [self sanitizeValue:[NSString stringWithFormat:@"%i", [self.ASAScoresList indexOfObject:self.ASAScore.text]+1]];
    
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:formID forKey:@"formID"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:reconstructionAge forKey: @"reconstructionAge"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:plannedDischargeDate forKey:@"plannedDischargeDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:actualDischargeDate forKey:@"actualDischargedDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:bmi forKey:@"BMI"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:braSize forKey:@"braSize"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:comorbidCondition forKey:@"comorbidCondition"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:comorbidConditionType forKey:@"comorbidConditionType"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:comorbidConditionOthers forKey:@"comorbidConditionOthers"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:asaScore forKey: @"ASAScore"];
    
    //Peform the transition
    [self performSegueWithIdentifier:@"FromTab1ToTab2" sender:self];
}


- (IBAction)SaveTab2Data:(UIBarButtonItem *)sender {
    NSLog(@"%i", [[HDMSModelStorage storageObject].Breast_PreObject count]);
    NSString* surgeryDate = [self sanitizeValue:DateOfSurgery.text];
    NSString* hospitalStaylength = [self sanitizeValue:HospitalStayLength.text];
    NSString* mastectomyLeftSurgeryDate =[self sanitizeValue: MasectomyDatePrevLeft.text];
    NSString* mastectomyRightSurgeryDate = [self sanitizeValue:MasectomyDatePrevRight.text];
    NSString* breastBiopsyLeftDate =  [self sanitizeValue:BiopsyDatePrevLeft.text];
    NSString* breastBiopsyRightDate = [self sanitizeValue:BiopsyDatePrevRight.text];
    NSString* WLELeft = [self sanitizeValue:WLEDatePrevLeft.text];
    NSString* WLERight = [self sanitizeValue:WLEDatePrevRight.text];
    NSString* breastExcisionLeftDate = [self sanitizeValue:MasectomyDatePrevLeft.text];
    NSString* breastExcisionRightDate = [self sanitizeValue:MasectomyDatePrevRight.text];

    [[HDMSModelStorage storageObject].Breast_PreObject setObject:surgeryDate forKey:@"surgeryDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:hospitalStaylength forKey:@"hospitalStaylength"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:mastectomyLeftSurgeryDate forKey:@"mastectomyLeftSurgeryDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:mastectomyRightSurgeryDate forKey:@"mastectomyRightSurgeryDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:breastBiopsyLeftDate forKey:@"breastBiopsyLeftDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:breastBiopsyRightDate forKey:@"breastBiopsyRightDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:WLELeft forKey:@"WLELeft"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:WLERight forKey:@"WLERight"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:breastExcisionLeftDate forKey:@"breastExcisionLeftDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:breastExcisionRightDate forKey:@"breastExcisionRightDate"];

    [self performSegueWithIdentifier:@"FromTab2ToTab3" sender:self];
}

- (IBAction)SaveTab3Data:(UIBarButtonItem *)sender {
    NSLog(@"%i", [[HDMSModelStorage storageObject].Breast_PreObject count]);
    
    NSString* lump = [self sanitizeValue:[self GetStringFromInteger:LumpPresent.selectedSegmentIndex]];
    NSString* areaOfThickening = [self sanitizeValue:[self GetStringFromInteger: AreaThickeningState.selectedSegmentIndex]];
    NSString* dimpleInTheBreast = [self sanitizeValue:[self GetStringFromInteger: DimpleBreastState.selectedSegmentIndex]];
    NSString* breastSwelling = [self sanitizeValue:[self GetStringFromInteger:BreastSwelling.selectedSegmentIndex]];
    NSString* redness = [self sanitizeValue:[self GetStringFromInteger:RednessState.selectedSegmentIndex]];
    NSString* enlargedUnderarm = [self sanitizeValue:[self GetStringFromInteger:EnlargedLymphNodeState.selectedSegmentIndex]];
    NSString* mastectomyPosition = [self sanitizeValue:[self GetStringFromInteger:MasectomyPosition.selectedSegmentIndex]];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:lump forKey:@"lump"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:areaOfThickening forKey:@"areaOfThickening"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:dimpleInTheBreast forKey:@"dimpleInTheBreast"];    
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:breastSwelling forKey:@"breastSwelling"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:redness forKey:@"redness"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:enlargedUnderarm forKey:@"enlargedUnderarm"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:mastectomyPosition forKey:@"mastectomyPosition"];
    
    [self performSegueWithIdentifier:@"FromTab3ToTab4" sender:self];
}

- (IBAction)SaveTab4Data:(id)sender {
    NSLog(@"%i", [[HDMSModelStorage storageObject].Breast_PreObject count]);

    NSString* IDC = [self sanitizeValue:[self GetGradeFromInteger:IDCGrade.text]];
    NSString* DCIS = [self sanitizeValue:[self GetGradeFromInteger:DCISGrade.text]];
    NSString* EDCinSitu = [self sanitizeValue:[self GetGradeFromInteger:ExternalDCISGrade.text]];
    NSString* IfDC = [self sanitizeValue:[self GetGradeFromInteger:InfiltrativeDuctalCarcinoma.text]];
    NSString* MC = [self sanitizeValue:[self GetGradeFromInteger:MucinousCarcinoma.text]];
    NSString* fibroadennomata = [self sanitizeValue:[self GetStringFromInteger:FibroadenomataPresence.selectedSegmentIndex]];
    NSString* fatNecrosis = [self sanitizeValue:[self GetStringFromInteger:FatNecrosisPresence.selectedSegmentIndex]];
    NSString* sarcoma = [self sanitizeValue:[self GetStringFromInteger:SacromaPresence.selectedSegmentIndex]];
    NSString* RBC = [self sanitizeValue:[self GetStringFromInteger:RecurrentBreastCarcinomaPresence.selectedSegmentIndex]];
    NSString* otherForMastectomy = [self sanitizeValue:self.MasectomyOtherReason.text];
    
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:IDC forKey:@"IDC"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:DCIS forKey:@"DCIS"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:EDCinSitu forKey:@"EDCinSitu"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:IfDC forKey:@"IfDC"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:MC forKey:@"MC"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:fibroadennomata forKey:@"fibroadennomata"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:fatNecrosis forKey:@"fatNecrosis"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:sarcoma forKey:@"sarcoma"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:RBC forKey:@"RBC"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:otherForMastectomy forKey:@"otherForMastectomy"];
    
    [self performSegueWithIdentifier:@"FromTab4ToTab5" sender:self];
}

- (IBAction)SaveTab5Data:(id)sender {
}
- (IBAction)Tab4GradeSelected:(UISegmentedControl *)sender {
    current_item_change_id = sender.tag;
        if(sender.selectedSegmentIndex == 1){
            self.pickerView = [self constructPickerViewWithTag:sender.tag];
            self.pickerView.hidden = NO;
        }
}

/// Request Stuff

/*******************************
 * SAMPLE SCHEMA FOR JSON DATA
 {
 "formID": "form ID",
 "reconstructionAge" : "The age of the patient", (integer/ must have)
 "plannedDischargeDate" : "The date that the patient is planned to be discharged"(yyyy-mm-dd)
 "actualDischargedDate : "The date that the patient is planned to be discharged"(yyyy-mm-dd)
 "BMI" : "BMI of the patient", (float)
 "braSize" : "bra size of the patient" (string)
 "comorbidCondition" : "Whether there are any pre existing medical condition/combid conditions of the patient", (boolean/must have)
 "comorbidConditionType" : "The comorbid condition that the patient have.", (integer)(1:COPD 2:DM Type I 3:CHF/CCF 4:CVA 5:IHD 6:Hypertension)
 "comorbidConditionOthers" : "Other comorbid condition that the patient have." (String)
 "ASAScore" : "The patient's ASA score", (Integer)(1: ASA I 2: ASA II 3:ASA III 4:ASA IV 5:ASA V)
 "surgeryDate" : "The date on which the surgery is conducted", (yyyy-mm-dd)
 "hospitalStaylength" : "How many days the patient stay in the hospital" (integer)
 "mastectomyLeftSurgeryDate" : "The date on which the mastactomy left surgery is conducted", (yyyy-mm-dd)
 "mastectomyRightSurgeryDate" : "The date on which the mastactomy right surgery is conducted", (yyyy-mm-dd)
 "breastBiopsyLeftDate" : "If yes. The date on which left breast biopsyis conducted, otherwise null", (yyyy-mm-dd)
 "breastBiopsyRightDate" : "If yes. The date on which right breast biopsy is conducted, otherwise null", (yyyy-mm-dd)
 "WLELeft" : "If yes, The date on which left Wide Local Excision is conducted, otherwise null", (yyyy-mm-dd)
 "WLERight" : "The date on which right Wide Local Excision is conducted, otherwise null", (yyyy-mm-dd)
 "breastExcisionLeftDate" : "The date on which left breast excision is conducted", (yyyy-mm-dd)
 "breastExcisionRightDate" : "The date on which second right breast excision is conducted", (yyyy-mm-dd)
 //"symptomsPresented" : "What is the sympthoms of the patient after excision", (Integer) (6 bits digits for Lump, AreaOfThicking, DimpleInTheBreast, BreastSwelling, Redness and EnlargeUnderarmLymphNode. 111111: all symptoms are presented)
 "lump" : "lump symptom presented", (boolean) (default false)
 "areaOfThickening" : "Area of thickening symptom presented", (boolean) (default false)
 "dimpleInTheBreast" : "dimple in the breast symptom presented", (boolean) (default false)
 "breastSwelling" : breast swelling symptom presented", (boolean) (default false)
 "redness" : "redness symptom presented", (boolean) (default false)
 "enlargedUnderarm" : "The patient's underarm is enlarged", (boolean) (default false)
 "mastectomyPosition" : "On which side is the patient going to do mastectomy", (Integer) (0: left 1: right 2: both) 
 //"mastectomyOtherReason" : "Other reason for patient's mastectomy", (String)
 "IDC" : "Invasive Ductual Carcinoma grade",(Integer)(1: grade 1 2:grade 2 3:grade 3)
 "DCIS" : "DCIS grade",(Integer)(1: grade 1 2:grade 2 3:grade 3)
 "EDCinSitu" : "Extensive Ductual Carcinoma in-situ grade",(Integer)(1: grade 1 2:grade 2 3:grade 3)
 "IfDC" : "Infiltrative Ductal Carcinoma in-situ grade",(Integer)(1: grade 1 2:grade 2 3:grade 3)
 "MC" : "Mucinous Carcinoma grade",(Integer)(1: grade 1 2:grade 2 3:grade 3)
 "fibroadennomata" : "patient has fibroadennomata?" (boolean)
 "fatNecrosis":"patient has fat necrosis?"(boolean)
 "sarcoma":"patient has sarcoma ?"(boolean)
 "RBC":"patient has recurrent breast carcinoma?" (boolean)
 "otherForMastectomy"："other reason for mastectomy"(String)
 "mammogramShowed" : "Result for the patient's mammogram", (String)
 "DoneDate" : "The date that the surgery is done", (yyyy-mm-dd)
 "XRayShowed" : "The result for the X-Ray test", (String)
 "XRayDate" : "The date that the x-ray is done", (yyyy-mm-dd)
*/
- (NSString*) GetStringFromInteger: (NSInteger) integer
{
    return [NSString stringWithFormat:@"%i", integer];
}

- (NSString*) GetGradeFromInteger: (NSString*) grade
{
    NSInteger index = [self.GradesList indexOfObject:grade] + 1;
    return [self GetStringFromInteger:index];
}

- (NSString*) sanitizeValue: (NSString*) stringWithissue
{
    //[CommonUtils StandardDebugLogWithString:stringWithissue];
    if (stringWithissue == nil || stringWithissue == NULL) {
        return @"";
    }
    return stringWithissue;
}

-(void) saveValues
{
        
    NSString* mammogramShowed = [self sanitizeValue:self.MammogramShowed.text];
    NSString* DoneDate = [self sanitizeValue:self.MammogramDateDone.text];
    NSString* xRayShowed = [self sanitizeValue:self.XRayShowed.text];
    NSString* xRayDate = [self sanitizeValue:self.XRayDateDone.text];
    
    
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:mammogramShowed forKey:@"mammogramShowed"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:DoneDate forKey:@"DoneDate"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:xRayShowed forKey: @"XRayShowed"];
    [[HDMSModelStorage storageObject].Breast_PreObject setObject:xRayDate forKey:@"XRayDate"];
    
    
    /*NSArray* values = [[NSArray alloc]initWithObjects: formID, reconstructionAge, plannedDischargeDate, actualDischargeDate, bmi, braSize, comorbidCondition, comorbidConditionType,  comorbidConditionOthers, asaScore, surgeryDate, hospitalStaylength, mastectomyLeftSurgeryDate, mastectomyRightSurgeryDate,breastBiopsyLeftDate ,breastBiopsyRightDate , WLELeft, WLERight , breastExcisionLeftDate , breastExcisionRightDate, lump, areaOfThickening, dimpleInTheBreast, breastSwelling, redness, enlargedUnderarm, mastectomyPosition, IDC, DCIS, EDCinSitu, IfDC, MC, fibroadennomata, fatNecrosis, sarcoma, RBC, otherForMastectomy, mammogramShowed, DoneDate, xRayShowed, xRayDate , nil];
    NSArray* keys = [[NSArray alloc]initWithObjects:@"formID", @"reconstructionAge", @"plannedDischargeDate", @"actualDischargedDate", @"BMI", @"braSize", @"comorbidCondition", @"comorbidConditionType", @"comorbidConditionOthers", @"ASAScore", @"surgeryDate",@"hospitalStaylength",@"mastectomyLeftSurgeryDate",@"mastectomyRightSurgeryDate",@"breastBiopsyLeftDate",@"breastBiopsyRightDate",@"WLELeft",@"WLERight",@"breastExcisionLeftDate",@"breastExcisionRightDate",@"lump",@"areaOfThickening",@"dimpleInTheBreast",@"breastSwelling",@"redness",@"enlargedUnderarm",@"mastectomyPosition",@"IDC",@"DCIS",@"EDCinSitu",@"IfDC",@"MC",@"fibroadennomata",@"fatNecrosis",@"sarcoma",@"RBC",@"otherForMastectomy",@"mammogramShowed",@"DoneDate",@"XRayShowed",@"XRayDate",  nil];
    */
    
    // MAKE REQUEST HERE
    
    NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_PreObject];

    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    
    [[RKClient sharedClient] post:[Constants BreastPreAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if([request isPOST])
    {
        if([response statusCode] == 201)
        {
            [CommonUtils DisplayModalPopupWithMessage:@"Request Succeeded"];
            [CommonUtils StandardDebugLogWithString:[response bodyAsString]];
        }
        else {
            [CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat: @"Request Did not Succeed %@", [response bodyAsString]]];
        }
    }
}

@end

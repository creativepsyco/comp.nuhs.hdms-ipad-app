//
//  HDMSModelStorage.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HDMSModelStorage.h"

@implementation HDMSModelStorage
static HDMSModelStorage* storageObject;
@synthesize UserObject = _UserObject;
@synthesize Config = _Config;
@synthesize OfflineDAO = _OfflineDAO;
@synthesize libraryContent = _libraryContent;
@synthesize PatientObject = _PatientObject;
@synthesize FormObject = _FormObject;
@synthesize Disease_PreObject = _Disease_PreObject;
@synthesize Disease_PostObject = _Disease_PostObject;
@synthesize Disease_IntraObject = _Disease_IntraObject;
@synthesize Breast_PreObject = _Breast_PreObject;
@synthesize Breast_IntraObject = _Breast_IntraObject;
@synthesize Breast_PostObject = _Breast_PostObject;
@synthesize FormMode = _FormMode;
@synthesize Breast_OldIntraObject;
@synthesize Breast_OldPreObject;
@synthesize Breast_OldPostObject;
@synthesize Diff_Object;

// Some constants for the Dictionary access
const NSString * user_name_key = @"user_name";
const NSString * user_email_key = @"user_email";
const NSString * user_userid_key = @"user_userid";
const NSString * user_access_token_key = @"user_access_token";
const NSString * user_access_level_key = @"user_access_level";

// Class Methods
+ (HDMSModelStorage*) storageObject 
{
    return storageObject;
}

+ (void) initialize {
    if ([HDMSModelStorage class] == self) {
        storageObject = [self new];
        [storageObject performCustomClassInit];
    }
}

+(id)allocWithZone: (NSZone*)aZone
{
    if(storageObject && [HDMSModelStorage class] == self)
    {
        [NSException raise:NSGenericException format:@"Cannot Create more than one object of the singleton"];
    }
    return [super allocWithZone:aZone];
}

// Instance methods
// Use this method for custom initialization of the 
// different stuff classes that we are using

-(void) performCustomClassInit
{
    // Init the user object model
    _UserObject = [[HDMSUser alloc]init];
    _Config = [[HDMSAppConfig alloc]init];
    //For Offline Data Access
    _OfflineDAO = [[OfflineDataAccess alloc]init]; 
    
    // TODO: initialize the rest of the objects with the offline data
    // The convention for keys is to be prefixed by object class name
    // e.g access_token becomes = user_access_token 
    
    // Initializing user object values
    
    _UserObject.name = [_OfflineDAO.libraryContent objectForKey:user_name_key];
    _UserObject.access_token = [_OfflineDAO.libraryContent objectForKey:user_access_token_key];
    _UserObject.email = [_OfflineDAO.libraryContent objectForKey:user_email_key];
    _UserObject.userid = [_OfflineDAO.libraryContent objectForKey:user_userid_key];
    _UserObject.access_level = [_OfflineDAO.libraryContent objectForKey:user_access_level_key];
    
    _PatientObject = [[NSDictionary alloc]init];
    _FormObject = [[NSDictionary alloc]init];
    _Disease_PreObject = [[NSMutableDictionary alloc]init];
    _Disease_IntraObject = [[NSMutableDictionary alloc]init];
    _Disease_PostObject = [[NSMutableDictionary alloc]init];
    
    _Breast_PreObject = [[NSMutableDictionary alloc]init];
    _Breast_PostObject = [[NSMutableDictionary alloc]init];
    _Breast_IntraObject = [[NSMutableDictionary alloc]init];
    
    Breast_OldPreObject = [[NSMutableDictionary alloc]init];
    Breast_OldPostObject = [[NSMutableDictionary alloc]init];
    Breast_OldIntraObject = [[NSMutableDictionary alloc]init];
    
    _FormMode = FORM_MODE_DEFAULT;
    Diff_Object = [[NSMutableDictionary alloc]init];
}

- (NSString*) sanitizeValueForObject: (NSString*) object
{
    if (object || object!= nil){
        return object;
    } else {
        return @"";
    }
}

-(void) saveOffline
{
    // Saves the stuff into the offline stuff
    [_OfflineDAO.libraryContent setObject: [self sanitizeValueForObject: _UserObject.userid] forKey:user_userid_key];
    [_OfflineDAO.libraryContent setObject: [self sanitizeValueForObject: _UserObject.access_token] forKey:user_access_token_key];
    [_OfflineDAO.libraryContent setObject: [self sanitizeValueForObject: _UserObject.name] forKey:user_name_key];
    [_OfflineDAO.libraryContent setObject: [self sanitizeValueForObject: _UserObject.email] forKey:user_email_key];
    [_OfflineDAO.libraryContent setObject: [self sanitizeValueForObject: _UserObject.access_level] forKey:user_access_level_key];
    
    // Call the actual offline saving method
    [_OfflineDAO saveOffline];
}

@end

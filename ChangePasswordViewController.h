//
//  ChangePasswordViewController.h
//  HDMS iPad App
//
//  Created by Xinyu Li on 28/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface ChangePasswordViewController : UIViewController<RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *OldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *NewPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ConfirmPasswordTextFiled;
- (IBAction)ChangePasswordAction:(id)sender;
- (IBAction)ClearFiledsAction:(id)sender;

@end

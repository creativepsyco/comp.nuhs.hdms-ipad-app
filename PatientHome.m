//
//  PatientHome.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PatientHome.h"
#import "PatientDetailSplitViewController.h"

@interface PatientHome ()

@end

@implementation PatientHome

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)PatientDetail:(id)sender {
    
    // Goto Patient Detail
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
        PatientDetailSplitViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PatientDetailSplitView"];
    self.splitViewController.viewControllers = vc.viewControllers;
}

/*- (void)loadView
{
    // If you create your views manually, you MUST override this method and use it to create your views.
    // If you use Interface Builder to create your views, then you must NOT override this method.
}*/

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end

//
//  PatientHistoryViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 28/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restkit/RestKit.h"
#import "ManagePatientMenuViewController.h"
#import "CommonUtils.h"


@interface PatientHistoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ResultsTableView;
@property (weak, nonatomic) IBOutlet UILabel *PatientBirthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *PatientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *PatientIDLabel;
- (IBAction)DeleteHistoryRecordAction:(id)sender;
- (IBAction)AddNewHistoryRecordAction:(id)sender;
- (IBAction)ViewFormHistoryAction:(id)sender;

@end

//
//  FormSelectPatientViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 21/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FormSelectPatientViewController.h"
#import "PatientDetailSplitViewController.h"

@interface FormSelectPatientViewController ()

@end

@implementation FormSelectPatientViewController
@synthesize tableFormResults;
@synthesize keyword;
@synthesize tableResults;
@synthesize PatientID;
@synthesize DiseaseName;
@synthesize DateOfAdmission;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setKeyword:nil];
    [self setTableResults:nil];
    [self setPatientID:nil];
    [self setDiseaseName:nil];
    [self setDateOfAdmission:nil];
    [self setTableFormResults:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)CreateFormAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    PatientDetailSplitViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PatientDetailSplitView"];
    self.splitViewController.viewControllers = vc.viewControllers;
}

- (IBAction)SearchPatientAction:(id)sender {
}
- (IBAction)EditFormAction:(id)sender {
}
@end

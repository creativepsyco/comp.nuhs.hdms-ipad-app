//
//  AddFormViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddFormViewController.h"
#import "Constants.h"
#import "RestKit/RestKit.h"
#import "CommonUtils.h"
#import "RestKit/RKRequestSerialization.h"

@interface AddFormViewController ()

@end

@implementation AddFormViewController
@synthesize PatientID;
@synthesize PatientName;
@synthesize DiseaseType;
@synthesize SelectedDate;
@synthesize datePicker;
@synthesize datelabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initializeWithDefaultPatient];
    [self initDateStuff];
    
}

- (void)viewDidUnload
{
    [self setPatientID:nil];
    [self setPatientName:nil];
    [self setDiseaseType:nil];
    [self setSelectedDate:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];  
    SelectedDate.text = [NSString stringWithFormat:@"%@",
                      [df stringFromDate:[NSDate date]]];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(LabelChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

-(void) initializeWithDefaultPatient
{
    if([HDMSModelStorage storageObject].PatientObject) {
        PatientID.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"];
        PatientName.text = [[HDMSModelStorage storageObject].PatientObject objectForKey:@"name"];
    }
}
- (void)LabelChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    SelectedDate.text = [NSString stringWithFormat:@"%@",
                      [df stringFromDate:datePicker.date]];
}


- (IBAction)ShowSelectDatePopOver:(id)sender {
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)AddFormAction:(id)sender {
    int selectedIndex = [self.DiseaseType selectedSegmentIndex];
    NSString *selectedDisease = [Constants GetDiseaseNameById:selectedIndex];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.PatientID.text, @"patientID", 
                            self.SelectedDate.text, @"admissionDate",
                            selectedDisease, @"disease",
                            nil];
    
    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    [CommonUtils SwitchOnRestKitDebug];
    [[RKClient sharedClient] post:[Constants FormAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if([request isPOST])
    {
        if([response statusCode] == 201)
        {
            [CommonUtils DisplayModalPopupWithMessage:@"Request Succeeded"];
            [CommonUtils StandardDebugLogWithString:[response bodyAsString]];
        }
        else {
            [CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat: @"Request Did not Succeed %@", [response bodyAsString]]];
        }
    }
}
@end

//
//  BreastPreEditTab3ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"
#import "HDMSModelStorage.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "Constants.h"

@interface BreastPreEditTab3ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}

// Tab3 Edit Fields
@property (weak, nonatomic) IBOutlet UISegmentedControl *LumpPresent;
@property (weak, nonatomic) IBOutlet UISegmentedControl *BreastSwelling;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AreaThickeningState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *RednessState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *DimpleBreastState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *EnlargedLymphNodeState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *MasectomyPosition;
- (IBAction)NavigateToTab4:(UIBarButtonItem *)sender;

@end

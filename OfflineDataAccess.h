//
//  OfflineDataAccess.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfflineDataAccess : NSObject
{
    NSString *libraryPlist;
    NSDictionary *libraryContent;
}
@property (nonatomic, retain) NSString *libraryPlist;
@property (nonatomic, retain) NSMutableDictionary *libraryContent;
- (void)saveOffline;
@end

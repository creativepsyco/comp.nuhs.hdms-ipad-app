//
//  BreastPreEditTab5ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreEditTab5ViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"

@interface BreastPreEditTab5ViewController ()
{
    int current_item_change_id; // Describes the tag property of the item being changed    
}

@end

@implementation BreastPreEditTab5ViewController
@synthesize SubmitBreastPreDataButton;
@synthesize SelectMammogramDateButton;
@synthesize SelectXRayDateButton;
@synthesize MammogramShowed;
@synthesize MammogramDateDone;
@synthesize XRayShowed;
@synthesize XRayDateDone;

// Pickers
@synthesize pickerView, datePicker, ASAScoresList, ComorbidConditionsList, GradesList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self initPickerArrays];
	[self initDateStuff];
    
	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
}

- (void)viewDidUnload
{
    [self setSubmitBreastPreDataButton:nil];
    [self setSelectMammogramDateButton:nil];
    [self setSelectXRayDateButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


// Stuff that is loaded just after start comes here

-(void) initPickerArrays
{
    self.ComorbidConditionsList = [[NSArray alloc]initWithObjects:@"COPD", @"DM Type I", @"CHF/CCF", @"CVA", @"IHD", @"Hypertension", @"Others", nil];
    self.ASAScoresList = [[NSArray alloc]initWithObjects:@"ASA I", @"ASA II", @"ASA III", @"ASA IV", @"ASA V", nil];
    self.GradesList = [[NSArray alloc] initWithObjects:@"Grade 1", @"Grade 2", @"Grade 3", nil];
}


- (void)initializeThePickerView {
    // Must display the UIPicker
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    self.pickerView.hidden = YES;
}

- (UIPickerView*) constructPickerViewWithTag: (int) tagNumber
{
    UIPickerView *myPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    myPicker.dataSource = self;
    myPicker.delegate = self;
    myPicker.showsSelectionIndicator = YES;
    myPicker.tag = tagNumber;
    [self.view addSubview:myPicker];
    myPicker.hidden = YES;
    return myPicker;
}

- (void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];      
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(DateChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

// is referred to by DatePicker on completion
- (void)DateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    NSString *myDate = [NSString stringWithFormat:@"%@",
                        [df stringFromDate:datePicker.date]];
    switch (current_item_change_id) {
            // Do stuff relevant to changing date of different labels
            
            // Tab 5
        case 501: MammogramDateDone.text = myDate; break;
        case 502: XRayDateDone.text = myDate; break;
        default:
            break;
    }
}



#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the stuff relevant to the picker view pressed 
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //For Comorbid
            return [self.ComorbidConditionsList count];
            break;
        case 104: //For ASA Score
            return [self.ASAScoresList count];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList count]; break;
        default:
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            return [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            return [self.ASAScoresList objectAtIndex:row];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    return @"NONE";
} 

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        default:
            break;
    }
    thePickerView.hidden = YES;
}

- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)SubmitBreastPreData:(UIBarButtonItem *)sender {
    [self SaveDataIntoModel];
    
    if([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW || [HDMSModelStorage storageObject].FormMode == FORM_MODE_DEFAULT )
    {
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_PreObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastPreAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    } else if ([[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"1"] && [HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
        //Admin: Can do anything, just need to send it to the right url
        /*!
         This mode allows admins to send their updates to the server.
         */
            
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_PreObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastPreUpdateURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];

    } else if ([[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"2"] && [HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
        /*!
         Here need to calculate the difference.
         */
        NSLog(@"Making a Modification Access Request");
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        
        NSDictionary *diff =  [CommonUtils CalculateDifferenceOf:[[HDMSModelStorage storageObject].Breast_OldPreObject copy] With:[[HDMSModelStorage storageObject].Breast_PreObject copy]];
        //        NSString* dict1String = [parser stringFromObject:[diff objectForKey:@"old"] error:&error ];
        //        NSString* dict2String = [parser stringFromObject:[diff objectForKey:@"new"] error:&error ];
        
        NSString* dict1String = [parser stringFromObject:[HDMSModelStorage storageObject].Breast_OldPreObject error:&error ];
        NSString* dict2String = [parser stringFromObject:[HDMSModelStorage storageObject].Breast_PreObject error:&error ];
        
        NSDictionary *params = [[NSDictionary alloc]initWithObjectsAndKeys: dict1String, @"oldFormData",dict2String, @"newFormData", @"0", @"type",
                                [[HDMSModelStorage storageObject].FormObject objectForKey: @"formID"], @"formid",
                                [HDMSModelStorage storageObject].UserObject.userid, @"userid", nil] ;
        
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants RequestModificationAccessURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    } else {
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_PreObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastPreUpdateURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    }
}


/*!
 End of the picker view and date code
 */

/// UI Control Management Region
/*!
 Used to check the sanity of the input
 Returns True if string is not empty
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}
/*!
 * Loads the dictionary data into view
 */
-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    [MammogramShowed setText:[mydir objectForKey:@"mammogramShowed"]];
    [MammogramDateDone setText:[mydir objectForKey:@"doneDate"]];
    [XRayShowed setText:[mydir objectForKey:@"XRayShowed"]];
    [XRayDateDone setText:[mydir objectForKey:@"XRayDate"]];
}
/**!
 * For sanitizing empty values
 */
- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Pre Object
	[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
}
/*!
 * Save Data into Model
 */
-(void) SaveDataIntoModel
{
    NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    NSString *mammogramShowed = MammogramShowed.text;
    NSString *mammogramDate = MammogramDateDone.text;
    NSString *xrayShowed = XRayShowed.text;
    NSString *xrayDate = XRayDateDone.text;
    
    
    [self SanitizeAndStoreValue:formID forKey:@"formID"];
    [self SanitizeAndStoreValue:mammogramShowed forKey:@"mammogramShowed"];
    [self SanitizeAndStoreValue:mammogramDate forKey:@"doneDate"];
    [self SanitizeAndStoreValue:xrayShowed forKey:@"XRayShowed"];
    [self SanitizeAndStoreValue:xrayDate forKey:@"XRayDate"];
    
    NSLog([[HDMSModelStorage storageObject].Breast_PreObject description]);
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    MammogramShowed.userInteractionEnabled = TRUE;
    SelectMammogramDateButton.hidden = FALSE;
    XRayShowed.userInteractionEnabled = TRUE;
    SelectXRayDateButton.hidden = FALSE;
}

-(void) EnablePartialControls
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"mammogramShowed"]])
        MammogramShowed.userInteractionEnabled = FALSE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"doneDate"]])
        SelectMammogramDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"XRayShowed"]])
        XRayShowed.userInteractionEnabled = FALSE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"XRayDate"]])
        SelectXRayDateButton.hidden = TRUE;
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if([request isPOST])
    {
        if([response statusCode] == 201)
        {
            [CommonUtils DisplayModalPopupWithMessage:@"Successfully posted to the server"];
            [CommonUtils StandardDebugLogWithString:[response bodyAsString]];
        }
        else {
            [CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat: @"Request Did not Succeed %@", [response bodyAsString]]];
        }
    }
}

@end

//
//  BreastPreEditTab2ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastPreEditTab2ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}

//  Common Actions
- (IBAction)SelectDateAction:(id)sender;
- (IBAction)NavigateToNextTab:(UIBarButtonItem *)sender;

//  Common Actions End

// Picker Controls and Arrays

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray* ASAScoresList;
@property (nonatomic, retain) NSArray* ComorbidConditionsList;
@property (nonatomic, retain) UIDatePicker* datePicker;
@property (nonatomic, retain) NSArray* GradesList;

@property (weak, nonatomic) IBOutlet UIButton *SelectSurgeryDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectLeftExcisionDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectRightExcisionDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectLeftBiopsyDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectRightBiopsyDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectLeftWLEDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectRightWLEDateButton;

// Tab 2 Edit Fields
@property (weak, nonatomic) IBOutlet UILabel *DateOfSurgery;
@property (weak, nonatomic) IBOutlet UITextField *HospitalStayLength;
@property (weak, nonatomic) IBOutlet UILabel *MasectomyDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *MasectomyDatePrevRight;
@property (weak, nonatomic) IBOutlet UILabel *BiopsyDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *BiopsyDatePrevRight;
@property (weak, nonatomic) IBOutlet UILabel *WLEDatePrevLeft;
@property (weak, nonatomic) IBOutlet UILabel *WLEDatePrevRight;

@end

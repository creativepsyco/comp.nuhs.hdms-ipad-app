//
//  UserSettingsViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"
@interface UserSettingsViewController : UIViewController <RKRequestDelegate>

//Password change
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *NewPasswordTextField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;

//User Info
@property (weak, nonatomic) IBOutlet UITextField *updatedName;
@property (weak, nonatomic) IBOutlet UITextField *updatedEmail;
@end

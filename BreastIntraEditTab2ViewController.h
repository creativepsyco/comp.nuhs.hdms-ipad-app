//
//  BreastIntraEditTab2ViewController.h
//  HDMS iPad App
//
//  Created by Xinyu Li on 4/4/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreastIntraEditTab2ViewController : UIViewController

//3 sub views
@property (weak, nonatomic) IBOutlet UIView *ImplantBasedView;
@property (weak, nonatomic) IBOutlet UIView *AutologousReconstructionView;
@property (weak, nonatomic) IBOutlet UIView *BothView;

//2 segemented controls
@property (weak, nonatomic) IBOutlet UISegmentedControl *BreastReconProcedureSegmentatedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SubViewSegmentedControl;
- (IBAction)SubViewSegmentedControlValueChangedAction:(id)sender;

//sub view 1: ImplantBased
@property (weak, nonatomic) IBOutlet UISegmentedControl *ImplantBasedChoiceSegementedControl;
@property (weak, nonatomic) IBOutlet UITextField *ImplantBasedImplantSizeRTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *ImplantedBasedImplantSizedRTextField;
@property (weak, nonatomic) IBOutlet UITextField *ImplantBasedExpanderSize;

//sub view2: AutologousReconstructionView
@property (weak, nonatomic) IBOutlet UISegmentedControl *AutologousReconChoiceSegementedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AutologousReconPedicledChoiceSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AutologousReconFreeFlapSegementedControl;
@property (weak, nonatomic) IBOutlet UITextField *AutologousReconIschemicTimeTextField;
- (IBAction)AutologousReconIschemicTimeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *AutologousReconDIEPPerforatorsTextFiled;
@property (weak, nonatomic) IBOutlet UISegmentedControl *AutologousReconRecepientVesselsSegementedControl;

//sub view3: Both
@property (weak, nonatomic) IBOutlet UISegmentedControl *BothChoiceSegementedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *BothPedicledChoiceSegementedControl;
@property (weak, nonatomic) IBOutlet UITextField *BothPedicledImplantSizeTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *BothFreeFlapChoiceSegementedControl;
@property (weak, nonatomic) IBOutlet UITextField *BothFreeFlapImplantSizeTextField;


- (IBAction)GoToNextTab:(UIBarButtonItem *)sender;

@end

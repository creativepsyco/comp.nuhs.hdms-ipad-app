//
//  ManagePatientMenuViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagePatientMenuViewController : UITableViewController

- (void) showUpdatePatientViewWithPatient: (NSDictionary*) patient;

@end

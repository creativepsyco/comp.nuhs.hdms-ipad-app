//
//  BreastPreEditTab2ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreEditTab2ViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"
@interface BreastPreEditTab2ViewController ()
{
    int current_item_change_id; // Describes the tag property of the item being changed
}
@end

@implementation BreastPreEditTab2ViewController
@synthesize SelectSurgeryDateButton;
@synthesize SelectLeftExcisionDateButton;
@synthesize SelectRightExcisionDateButton;
@synthesize SelectLeftBiopsyDateButton;
@synthesize SelectRightBiopsyDateButton;
@synthesize SelectLeftWLEDateButton;
@synthesize SelectRightWLEDateButton;
@synthesize DateOfSurgery;
@synthesize HospitalStayLength;
@synthesize MasectomyDatePrevLeft;
@synthesize MasectomyDatePrevRight;
@synthesize BiopsyDatePrevLeft;
@synthesize BiopsyDatePrevRight;
@synthesize WLEDatePrevLeft;
@synthesize WLEDatePrevRight;

// Stuff that is loaded just after start comes here

/*!
 Picker View and Date Selection Code
 */

@synthesize pickerView, datePicker, ASAScoresList, ComorbidConditionsList, GradesList;
-(void) initPickerArrays
{
    self.ComorbidConditionsList = [[NSArray alloc]initWithObjects:@"COPD", @"DM Type I", @"CHF/CCF", @"CVA", @"IHD", @"Hypertension", @"Others", nil];
    self.ASAScoresList = [[NSArray alloc]initWithObjects:@"ASA I", @"ASA II", @"ASA III", @"ASA IV", @"ASA V", nil];
    self.GradesList = [[NSArray alloc] initWithObjects:@"Grade 1", @"Grade 2", @"Grade 3", nil];
}



- (void)initializeThePickerView {
    // Must display the UIPicker
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    self.pickerView.hidden = YES;
}

- (UIPickerView*) constructPickerViewWithTag: (int) tagNumber
{
    UIPickerView *myPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    myPicker.dataSource = self;
    myPicker.delegate = self;
    myPicker.showsSelectionIndicator = YES;
    myPicker.tag = tagNumber;
    [self.view addSubview:myPicker];
    myPicker.hidden = YES;
    return myPicker;
}

- (void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];      
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(DateChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

// is referred to by DatePicker on completion
- (void)DateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    NSString *myDate = [NSString stringWithFormat:@"%@",
                        [df stringFromDate:datePicker.date]];
    switch (current_item_change_id) {
            // Do stuff relevant to changing date of different labels
                        
            // Second Tab
        case 201: DateOfSurgery.text = myDate; break;
        case 202: MasectomyDatePrevLeft.text = myDate; break;        
        case 203: MasectomyDatePrevRight.text = myDate; break;
        case 204: BiopsyDatePrevLeft.text = myDate; break;
        case 205: BiopsyDatePrevRight.text = myDate; break;
        case 206: WLEDatePrevLeft.text = myDate; break;
        case 207: WLEDatePrevRight.text = myDate; break;
        default : break;
    }
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the stuff relevant to the picker view pressed 
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //For Comorbid
            return [self.ComorbidConditionsList count];
            break;
        case 104: //For ASA Score
            return [self.ASAScoresList count];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList count]; break;
        default:
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            return [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            return [self.ASAScoresList objectAtIndex:row];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    return @"NONE";
} 

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        default:
            break;
    }
    thePickerView.hidden = YES;
}

/***
 * End of the picker view delegate region.
 */

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
	[self initPickerArrays];
	[self initDateStuff];
    
	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
    
}

- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)NavigateToNextTab:(UIBarButtonItem *)sender {
    [self SaveDataIntoModel];
    [self performSegueWithIdentifier:@"FromTab2ToTab3" sender:self];
}


- (void)viewDidUnload
{
    [self setSelectSurgeryDateButton:nil];
    [self setSelectLeftExcisionDateButton:nil];
    [self setSelectRightExcisionDateButton:nil];
    [self setSelectLeftBiopsyDateButton:nil];
    [self setSelectRightBiopsyDateButton:nil];
    [self setSelectLeftWLEDateButton:nil];
    [self setSelectRightWLEDateButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


/*!
 End of the picker view and date code
 */

/// UI Control Management Region
/*!
 Used to check the sanity of the input
 Returns True if string is not empty
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}
/*!
 * Loads the dictionary data into view
 */
-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    [DateOfSurgery setText:[mydir objectForKey:@"surgeryDate"]];
    [HospitalStayLength setText:[mydir objectForKey:@"hospitalStaylength"]];
    [MasectomyDatePrevLeft setText:[mydir objectForKey:@"breastExcisionLeftDate"]];
    [MasectomyDatePrevRight setText:[mydir objectForKey:@"breastExcisionRightDate"]];
    [BiopsyDatePrevLeft setText:[mydir objectForKey:@"breastBiopsyLeftDate"]];
    [BiopsyDatePrevRight setText:[mydir objectForKey:@"breastBiopsyRightDate"]];
    [WLEDatePrevLeft setText:[mydir objectForKey:@"WLELeft"]];
    [WLEDatePrevRight setText:[mydir objectForKey:@"WLERight"]];
}
/**!
 * For sanitizing empty values
 */
- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil && ![value isEqualToString:@"YYYY-MM-DD"]) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Pre Object
	[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
}
/*!
 * Save Data into Model
 */
-(void) SaveDataIntoModel
{
    NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    NSString *dateofsurgery = DateOfSurgery.text;
    NSString *hospitallength = HospitalStayLength.text;
    NSString *breastExcisionLeft = MasectomyDatePrevLeft.text;
    NSString *breastExcisionRight = MasectomyDatePrevRight.text;
    NSString *biopsyLeft = BiopsyDatePrevLeft.text;
    NSString *biopsyRight = BiopsyDatePrevRight.text;
    NSString *wleLeft = WLEDatePrevLeft.text;
    NSString *wleRight = WLEDatePrevRight.text;
    
    [self SanitizeAndStoreValue:formID forKey:@"formID"];
    [self SanitizeAndStoreValue:dateofsurgery forKey:@"surgeryDate"];
    [self SanitizeAndStoreValue:hospitallength forKey:@"hospitalStaylength"];
    [self SanitizeAndStoreValue:breastExcisionLeft forKey:@"breastExcisionLeftDate"];
    [self SanitizeAndStoreValue:breastExcisionRight forKey:@"breastExcisionRightDate"];
    [self SanitizeAndStoreValue:biopsyLeft forKey:@"breastBiopsyLeftDate"];
    [self SanitizeAndStoreValue:biopsyRight forKey:@"breastBiopsyRightDate"];
    [self SanitizeAndStoreValue:wleLeft forKey:@"WLELeft"];
    [self SanitizeAndStoreValue:wleRight forKey:@"WLERight"];
    NSLog([[HDMSModelStorage storageObject].Breast_PreObject description]);
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    SelectSurgeryDateButton.hidden = FALSE;
    SelectLeftBiopsyDateButton.hidden = FALSE;
    SelectLeftExcisionDateButton.hidden = FALSE;
    SelectLeftWLEDateButton.hidden = FALSE;
    SelectRightBiopsyDateButton.hidden = FALSE;
    SelectRightWLEDateButton.hidden = FALSE;
    SelectRightExcisionDateButton.hidden = FALSE;
    HospitalStayLength.userInteractionEnabled = TRUE;
}

-(void) EnablePartialControls
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"surgeryDate"]])
        SelectSurgeryDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"breastExcisionLeftDate"]])
        SelectLeftExcisionDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"breastExcisionRightDate"]])
        SelectRightExcisionDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"breastBiopsyLeftDate"]])
        SelectLeftBiopsyDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"breastBiopsyRightDate"]])
        SelectRightBiopsyDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"WLELeft"]])
        SelectLeftWLEDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"WLERight"]])
        SelectRightWLEDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"hospitalStaylength"]])
        HospitalStayLength.userInteractionEnabled = FALSE;
}

@end

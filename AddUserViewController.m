//
//  AddUserViewController.m
//  HDMS iPad App
//
//  Created by Xinyu Li on 27/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import "AddUserViewController.h"
#import "HDMSModelStorage.h"
#import "Constants.h"
#import "RestKit/RKRequestSerialization.h"
#import "RestKit/RestKit.h"

@interface AddUserViewController ()

@end

@implementation AddUserViewController
@synthesize UserIDTextField;
@synthesize NameTextField;
@synthesize EmailTextField;
@synthesize PasswordTextField;
@synthesize ConfirmPasswordTextField;
@synthesize AccessLevelSegment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setUserIDTextField:nil];
    [self setNameTextField:nil];
    [self setEmailTextField:nil];
    [self setPasswordTextField:nil];
    [self setConfirmPasswordTextField:nil];
    //[self setAccessLevelSegment:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)DisplayModalPopup:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
                          @"HDMS App"
                                                  message			:
                          message
                          delegate
                                                            :self cancelButtonTitle:
                          @"Close"
                                           otherButtonTitles:nil];
    
    [alert show];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    NSLog(@"Response Body%@", [response bodyAsString]);
    
	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[self DisplayModalPopup:@"Request Carried out successfully"];
            [self ClearFieldsAction:self];
		}
	
    }
}
    

- (IBAction)AddNewUserAction:(id)sender {
    if (![self.PasswordTextField.text isEqualToString:self.ConfirmPasswordTextField.text])
    {
        [self DisplayModalPopup:@"Your New Password and Confirm Password are not consistent."];
        self.PasswordTextField.text=@"";
        self.ConfirmPasswordTextField.text=@"";
        return;
    }
    
    
    NSString *access_token = [[HDMSModelStorage storageObject] UserObject].access_token;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.UserIDTextField.text, @"userid", 
                            self.NameTextField.text, @"name",
                            self.EmailTextField.text, @"email",
                            [NSString stringWithFormat:@"%i",self.AccessLevelSegment.selectedSegmentIndex], @"accessLevel",
                            self.PasswordTextField.text, @"password",
                            access_token, @"token",
                            nil];
    
	id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
	NSError			*error	= nil;
	NSString		*json	= [parser stringFromObject:params error:&error];
    
	[[RKClient sharedClient] post:[Constants UserAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (IBAction)ClearFieldsAction:(id)sender {
    self.UserIDTextField.text=@"";
    self.NameTextField.text=@"";
    self.EmailTextField.text=@"";
    self.PasswordTextField.text=@"";
    self.ConfirmPasswordTextField.text=@"";
    self.AccessLevelSegment.selectedSegmentIndex=0;
}
@end

//
//  HomeDetail.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restkit/RestKit.h"

@interface HomeDetail : UIViewController <RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorSpinner;

@end

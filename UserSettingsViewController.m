//
//  UserSettingsViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserSettingsViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "HDMSModelStorage.h"
#import "HDMSUser.h"
#import "Constants.h"

@interface UserSettingsViewController ()

@end

@implementation UserSettingsViewController
@synthesize oldPassword;
@synthesize NewPasswordTextField;

@synthesize confirmPassword;
@synthesize updatedName;
@synthesize updatedEmail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

/*- (void)loadView
 *   {
 *   // If you create your views manually, you MUST override this method and use it to create your views.
 *   // If you use Interface Builder to create your views, then you must NOT override this method.
 *   }*/
- (void)DisplayModalPopupWithMessage:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
		@"HDMS iPad App"						message			:
		message
		delegate
																:self cancelButtonTitle:
		@"Close"
		otherButtonTitles:nil];

	[alert show];
}

- (IBAction)ChangePassword:(id)sender
{
	if ([confirmPassword.text compare:NewPasswordTextField.text] != 0) {
		[self DisplayModalPopupWithMessage:@"The confirm password does not match with the entered password"];
		return;
	}

	// Now make the web request
	NSString		*password		= NewPasswordTextField.text;
	NSString		*old_password	= oldPassword.text;
	NSDictionary	*params			= [NSDictionary dictionaryWithObjectsAndKeys:
		password, @"password",
		old_password, @"oldPassword",
		[[HDMSModelStorage storageObject] UserObject].access_token, @"token",
		nil];

    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
	id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
	NSError			*error	= nil;
	NSString		*json	= [parser stringFromObject:params error:&error];

	[[RKClient sharedClient] post:UserPasswordChangeURL params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[self DisplayModalPopupWithMessage:@"Request Carried Out Successfully"];
		} else {
			[self DisplayModalPopupWithMessage:[NSString stringWithFormat:@"Request Failed to succed, HTTP status code %d", [response statusCode]]];
			NSLog(@"Response %@", [response bodyAsString]);
		}
	}
}

- (IBAction)ChangeUserInfo:(id)sender
{
    
	NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
		updatedName.text, @"name",
		updatedEmail.text, @"email",
		[[HDMSModelStorage storageObject] UserObject].access_token, @"token",
		nil];

	id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
	NSError			*error	= nil;
	NSString		*json	= [parser stringFromObject:params error:&error];

	[[RKClient sharedClient] post:UserInfoChangeURL params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[updatedName setText:[[HDMSModelStorage storageObject] UserObject].name];
	[updatedEmail setText:[[HDMSModelStorage storageObject] UserObject].email];
	NSLog(@"Name %@", [[HDMSModelStorage storageObject] UserObject].name);
}

- (void)viewDidUnload
{
	[self setOldPassword:nil];
	[super viewDidUnload];
	// Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationPortrait;
}

@end
//
//  BreastIntraEditTab1ViewController.h
//  HDMS iPad App
//
//  Created by Xinyu Li on 1/4/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastIntraEditTab1ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView *pickerView;
    UIDatePicker *datePicker;
    NSArray *surgicalProcedureList;
    NSArray *secondSurgicalProcedureList;
}
- (IBAction)NavigateToSecondTab:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UILabel *DateOfSurgeryTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SiteOfOperationSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *SurgicalProcedureTextField;
@property (weak, nonatomic) IBOutlet UITextField *FirstSurgeryWeight;

- (IBAction)SelectSurgicalProcedureAction:(id)sender;
- (IBAction)SelectDateOfSurgeryAction:(id)sender;
- (IBAction)RequestModificationAccessAction:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *RequestModificationAccessButton;

@property (weak, nonatomic) IBOutlet UIButton *SelectFirstSurgicalTextButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectFirstSurgeryDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectSecondSurgicalTextButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectSecondSurgicalDateText;

@property (weak, nonatomic) IBOutlet UISegmentedControl *SecondSurgicalPerformedSegmentedControl;
@property (weak, nonatomic) IBOutlet UIView *SecondSurgicalPerformedView;
- (IBAction)ndSurgicalPerformedAction:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *SecondSurgicalProcedureTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SecondSiteOfOperationSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *SecondDateOfSurgeryTextField;
@property (weak, nonatomic) IBOutlet UITextField *SecondSurgeryWeight;

- (IBAction)SecondSelectSurgicalProcedureAction:(id)sender;

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) NSArray* surgicalProcedureList;
@property (nonatomic, retain) NSArray* secondSurgicalProcedureList;

@end

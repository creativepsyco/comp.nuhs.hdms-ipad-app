//
//  ChangePasswordViewController.m
//  HDMS iPad App
//
//  Created by Xinyu Li on 28/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constants.h"
#import "RestKit/RKRequestSerialization.h"
#import "HDMSModelStorage.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize OldPasswordTextField;
@synthesize NewPasswordTextField;
@synthesize ConfirmPasswordTextFiled;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setNewPasswordTextField:nil];
    [self setOldPasswordTextField:nil];
    [self setConfirmPasswordTextFiled:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{    
	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[self ShowPopupDialogBoxWithMessage:@"Your Password Has Been Changed Successfully."];
            [self ClearFiledsAction:self];
		}
        else {
            [self ShowPopupDialogBoxWithMessage:[response bodyAsString]];
            [self ClearFiledsAction:self];
        }
    }
}

- (void) ShowPopupDialogBoxWithMessage: (NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self ApplicationTitle] 
                                                    message:message 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (NSString *)ApplicationTitle {
    return @"HDMS iPad Application";
}

- (IBAction)ChangePasswordAction:(id)sender {
    
    if (![self.NewPasswordTextField.text isEqualToString:self.ConfirmPasswordTextFiled.text])
    {
        [self ShowPopupDialogBoxWithMessage:@"Your New Password and Confirm Password are not consistent."];
        [self ClearFiledsAction:self];
    }
    else
    {
        NSString *token = [[HDMSModelStorage storageObject]UserObject].access_token;
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.OldPasswordTextField.text, @"oldPassword", 
                                self.NewPasswordTextField.text, @"password",
                                token, @"token",
                                nil];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants ChangePassword] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    }
}

- (IBAction)ClearFiledsAction:(id)sender {
    self.OldPasswordTextField.text=@"";
    self.NewPasswordTextField.text=@"";
    self.ConfirmPasswordTextFiled.text=@"";
}
@end

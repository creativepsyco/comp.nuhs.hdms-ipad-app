//
//  BreastPreEditTab1ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 15/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastPreEditTab1ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    // Arrays to store the stuff
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}

@property (weak, nonatomic) IBOutlet UITextField *ReconstructionAge;

@property (weak, nonatomic) IBOutlet UILabel *PlannedDischargeDate; //tag: 101

@property (weak, nonatomic) IBOutlet UILabel *ActualDischargeDate;
@property (weak, nonatomic) IBOutlet UITextField *BraSize;
@property (weak, nonatomic) IBOutlet UITextField *BMI;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ComorbidCondition;
@property (weak, nonatomic) IBOutlet UILabel *ComorbidConditionType;
@property (weak, nonatomic) IBOutlet UITextField *ComorbidConditionTypeOthers;
@property (weak, nonatomic) IBOutlet UILabel *ASAScore;

- (IBAction)SelectDateAction:(id)sender;
- (IBAction)SelectASAScore:(UIButton*)sender;
- (IBAction)ComorbidConditionChanged:(UISegmentedControl *)sender;
- (IBAction)NavigateToTab2:(UIBarButtonItem *)sender;
- (IBAction)RequestModificationAccess:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *RequestModificationAccessButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectDischargeDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectActualDischargeDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectASAScoreButton;

/*!
 Picker Views and Date selection
 */

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray* ASAScoresList;
@property (nonatomic, retain) NSArray* ComorbidConditionsList;
@property (nonatomic, retain) UIDatePicker* datePicker;
@property (nonatomic, retain) NSArray* GradesList;

@end

//
//  UpdateUserViewController.h
//  HDMS iPad App
//
//  Created by Xinyu Li on 27/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface UpdateUserViewController : UIViewController<RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UITextField *EmailTextField;
- (IBAction)UpdateUserAction:(id)sender;
- (void) initializeWithUser: (NSDictionary*) user;

@end

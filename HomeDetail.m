//
//  HomeDetail.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeDetail.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "Constants.h"
#import "HDMSModelStorage.h"

@interface HomeDetail ()

@end

@implementation HomeDetail
@synthesize activityIndicatorSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*- (void)loadView
{
    // If you create your views manually, you MUST override this method and use it to create your views.
    // If you use Interface Builder to create your views, then you must NOT override this method.
}*/

- (void) viewDidAppear:(BOOL)animated{

    // Send the web request
    NSString * userid = [[HDMSModelStorage storageObject]UserObject].userid;
    NSDictionary	*params		= [NSDictionary dictionaryWithObjectsAndKeys:
                                   userid, @"userid",
                                   [[HDMSModelStorage storageObject] UserObject].access_token, @"token",
                                   nil];
    // Only for Debugging purposes
    // RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    
    [[RKClient sharedClient] get:UserInfoGetURL queryParams:params delegate:self];
//    [[RKClient sharedClient] get:UserInfoGetURL queryParams:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    // Animate the spinner
    activityIndicatorSpinner.hidesWhenStopped = YES;
    [activityIndicatorSpinner startAnimating];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setActivityIndicatorSpinner:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

///// Utility FUNCTIONS ////
- (void)DisplayModalPopupWithMessage:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
                          @"HDMS iPad App" message	:
                          message
                          delegate
                                                            :self cancelButtonTitle:
                          @"Close"
                                           otherButtonTitles:nil];
    
	[alert show];
}


//////////////////////// Web Request Functions //////////////////
//

/**
 * Sent when a request has finished loading
 */
- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response{
    if ([request isGET]) {
        if (response.statusCode == 201){
            //Stop the spinner
            [activityIndicatorSpinner stopAnimating];
            // set the object data
            NSString * json = [response bodyAsString];
            id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
            NSError			*error	= nil;
            NSDictionary * results = [parser objectFromString:json error:&error];
            [self saveJsonGetInfoForObject: results];
            NSLog(@"%@ %@", [results objectForKey:@"name"], [results objectForKey:@"email"]);
        } else {
            [self DisplayModalPopupWithMessage:@"Could not complete the request. Check your internet connectivity"];
        }
    }
    NSLog(@"Response %@", [response bodyAsString]);
}

- (void) saveJsonGetInfoForObject: (NSDictionary*) object {
    [[HDMSModelStorage storageObject]UserObject].name = [object objectForKey:@"name"];
    [[HDMSModelStorage storageObject]UserObject].email = [object objectForKey:@"email"];
    [[HDMSModelStorage storageObject]UserObject].access_level = [object objectForKey:@"accessLevel"];
    [[HDMSModelStorage storageObject] saveOffline];
}

/**
 * Sent when a request has failed due to an error
 */
- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error{
    [self DisplayModalPopupWithMessage:@"Failure to load"];
}

/**
 * Sent when a request has started loading
 */
- (void)requestDidStartLoad:(RKRequest *)request{
    
}


@end

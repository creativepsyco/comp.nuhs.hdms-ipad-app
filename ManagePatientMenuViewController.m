//
//  ManagePatientMenuViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ManagePatientMenuViewController.h"
#import "HDMSModelStorage.h"
#import "AddPatientViewController.h"
#import "PatientSearchViewController.h"
#import "UpdatePatientViewController.h"

@interface ManagePatientMenuViewController ()

@end

@implementation ManagePatientMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
/*
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

*/
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Navigation logic may go here. Create and push another view controller.
	int rowSelected = indexPath.row;
	// int sectionSelected = indexPath.section;
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
	switch (rowSelected) {
	        
		case 0:
        {
            AddPatientViewController	*vc		= [storyboard instantiateViewControllerWithIdentifier:@"AddPatientView"];
            NSArray						*mArr	= self.splitViewController.viewControllers;
            NSArray						*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
            
            self.splitViewController.viewControllers = mArr2;
        }
			break;
        case 1:
        {
            PatientSearchViewController	*vc		= [storyboard instantiateViewControllerWithIdentifier:@"PatientSearchView"];
//            UINavigationController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PatientNavigationView"];
            //custom init
            //vc.patientMenu = self;
            NSArray						*mArr	= self.splitViewController.viewControllers;
            NSArray						*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
            
            self.splitViewController.viewControllers = mArr2;
        }
			break;
            
    }
}

#pragma mark - All the member functions
// Takes in shallow patient details
- (void) showUpdatePatientViewWithPatient: (NSDictionary*) patient
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];

    UpdatePatientViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"UpdatePatientView"];
    vc.patientMenu = self;
    [HDMSModelStorage storageObject].PatientObject = [patient copy];
    [vc initializeWithPatient:[HDMSModelStorage storageObject].PatientObject];
    
    NSArray						*mArr	= self.splitViewController.viewControllers;
    NSArray						*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
    self.splitViewController.viewControllers = mArr2;
}
@end

//
//  BreastIntraEditTab3ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 10/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastIntraEditTab3ViewController.h"
#import "CommonUtils.h"
#import "Constants.h"
#import "HDMSModelStorage.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"

@interface BreastIntraEditTab3ViewController ()

@end

@implementation BreastIntraEditTab3ViewController
@synthesize Others;
@synthesize OtherReason;
@synthesize StartTime;
@synthesize EndTime;
@synthesize OtherComments;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
/*!
 Used to check the sanity of the input
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{

        Others.userInteractionEnabled = TRUE;

        OtherReason.userInteractionEnabled = TRUE;

        StartTime.userInteractionEnabled = TRUE;

        EndTime.userInteractionEnabled = TRUE;

        OtherComments.userInteractionEnabled = TRUE;
}

-(void) EnablePartialControls
{
    
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"others"]])
        Others.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"choiceReason"]])
        OtherReason.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"startTime"]])
        StartTime.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"endTime"]])
        EndTime.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"comments"]])
        OtherComments.userInteractionEnabled = FALSE;
    
    // More Controls
}

-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
    [Others setText:[mydir objectForKey:@"others"]];
    [OtherReason setText:[mydir objectForKey:@"choiceReason"]];
    [StartTime setText:[mydir objectForKey:@"startTime"]];
    [EndTime setText:[mydir objectForKey:@"endTime"]];
    [OtherComments setText:[mydir objectForKey:@"comments"]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
}

- (void)viewDidUnload
{
    [self setOthers:nil];
    [self setOtherReason:nil];
    [self setStartTime:nil];
    [self setEndTime:nil];
    [self setOtherComments:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


-(NSString*) sanitizeValue: (NSString*) value
{
    if (value != nil) { 
        return value;
    }
    else {
        return @"";
    }
}

-(void) SanitizeAndStoreValue: (NSString*)value forKey: (NSString*) key
{
    // Only dealing with Breast Intra Object
    [[HDMSModelStorage storageObject].Breast_IntraObject setObject:[self sanitizeValue: value] forKey: key ];
}
-(void) saveValues
{
    NSString* others = Others.text;
    NSString* choiceReason = OtherReason.text;
    NSString* startTime = StartTime.text;
    NSString* endTime = EndTime.text;
    NSString* comments = OtherComments.text;
    
    [self SanitizeAndStoreValue:others forKey:@"others"];
    [self SanitizeAndStoreValue:choiceReason forKey:@"choiceReason"];
    [self SanitizeAndStoreValue:startTime forKey:@"startTime"];
    [self SanitizeAndStoreValue:endTime forKey:@"endTime"];
    [self SanitizeAndStoreValue:comments forKey:@"comments"];
}

- (IBAction)SubmitIntraData:(UIBarButtonItem *)sender {
    [self saveValues];
    
    if([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW || [HDMSModelStorage storageObject].FormMode == FORM_MODE_DEFAULT )
    {
        // Form is new so just submit
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_IntraObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastIntraAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    }
    
    else if ([[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"1"] && [HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
        //Admin: Can do anything, just need to send it to the right url
        /*!
         This mode allows admins to send their updates to the server.
         */
         
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_IntraObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastIntraUpdateURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
        
    }
    else if ([[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"2"] && [HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
        /*!
         Here need to calculate the difference.
         */
        NSLog(@"Making a Modification Access Request");
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        
        NSDictionary *diff =  [CommonUtils CalculateDifferenceOf:[[HDMSModelStorage storageObject].Breast_OldIntraObject copy] With:[[HDMSModelStorage storageObject].Breast_IntraObject copy]];
//        NSString* dict1String = [parser stringFromObject:[diff objectForKey:@"old"] error:&error ];
//        NSString* dict2String = [parser stringFromObject:[diff objectForKey:@"new"] error:&error ];

        NSString* dict1String = [parser stringFromObject:[HDMSModelStorage storageObject].Breast_OldIntraObject error:&error ];
        NSString* dict2String = [parser stringFromObject:[HDMSModelStorage storageObject].Breast_IntraObject error:&error ];
        
        NSDictionary *params = [[NSDictionary alloc]initWithObjectsAndKeys: dict1String, @"oldFormData",dict2String, @"newFormData", @"1", @"type",
                                [[HDMSModelStorage storageObject].FormObject objectForKey: @"formID"], @"formid",
                                [HDMSModelStorage storageObject].UserObject.userid, @"userid", nil] ;
        
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants RequestModificationAccessURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
    } else {
        NSDictionary *params = [[NSDictionary alloc] initWithDictionary:[HDMSModelStorage storageObject].Breast_IntraObject];
        
        id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
        NSError			*error	= nil;
        NSString		*json	= [parser stringFromObject:params error:&error];
        
        [[RKClient sharedClient] post:[Constants BreastIntraUpdateURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
        
    }
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if([request isPOST])
    {
        if([response statusCode] == 201)
        {
            [CommonUtils DisplayModalPopupWithMessage:@"Request Succeeded"];
            [CommonUtils StandardDebugLogWithString:[response bodyAsString]];
        }
        else {
            [CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat: @"Request Did not Succeed %@", [response bodyAsString]]];
        }
    }
}


@end

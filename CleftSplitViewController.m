//
//  CleftSplitViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CleftSplitViewController.h"

@interface CleftSplitViewController ()

@end

@implementation CleftSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

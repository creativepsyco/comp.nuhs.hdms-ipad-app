//
//  OfflineDataAccess.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfflineDataAccess.h"

@implementation OfflineDataAccess

NSString *const OFFLINE_STORAGE_PLIST_NAME = @"offlineStorage.plist";

@synthesize libraryPlist;
@synthesize libraryContent = _libraryContent;

- (id)init
{
	self = [super init];

	if (self) {
		NSString				*errorDesc = nil;
		NSPropertyListFormat	format;
		NSString				*plistPath;
		NSString				*rootPath = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory,
				NSUserDomainMask, YES) objectAtIndex:0];
		plistPath = [rootPath stringByAppendingPathComponent:OFFLINE_STORAGE_PLIST_NAME];

		if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
			plistPath = [[NSBundle mainBundle] pathForResource:@"offlineStorage" ofType:@"plist"];
		}

		NSData			*plistXML	= [[NSFileManager defaultManager] contentsAtPath:plistPath];
		NSDictionary	*temp		= (NSDictionary *)[NSPropertyListSerialization
			propertyListFromData:plistXML
			mutabilityOption	:NSPropertyListMutableContainersAndLeaves
			format				:&format
			errorDescription	:&errorDesc];

		if (!temp) {
			NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
		}

		self.libraryContent = [NSMutableDictionary dictionaryWithDictionary:temp];
	}

	return self;
}

- (void)saveOffline
{
	NSString		*error;
	NSString		*rootPath	= [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString		*plistPath	= [rootPath stringByAppendingPathComponent:@"offlineStorage.plist"];
	NSDictionary	*plistDict	= _libraryContent;
	NSData			*plistData	= [NSPropertyListSerialization	dataFromPropertyList:plistDict format: NSPropertyListXMLFormat_v1_0								errorDescription	:&error];

	if (plistData) {
		[plistData writeToFile:plistPath atomically:YES];
	} else {
		NSLog(@"Error occured in saving offline%@", error);
	}
}

@end
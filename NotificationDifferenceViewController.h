//
//  NotificationDifferenceViewController.h
//  HDMS iPad App
//
//  Created by msk on 18/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationDifferenceViewController : UITableViewController
@property (nonatomic, retain) NSDictionary* diff_object;
@end

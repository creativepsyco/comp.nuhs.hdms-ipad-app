//
//  BreastReconViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastReconViewController.h"

@interface BreastReconViewController ()

@end

@implementation BreastReconViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
// Stuff that needs to be done just after load comes here

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

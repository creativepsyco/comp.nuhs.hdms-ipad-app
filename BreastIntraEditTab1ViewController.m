//
//  BreastIntraEditTab1ViewController.m
//  HDMS iPad App
//
//  Created by Xinyu Li on 1/4/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import "BreastIntraEditTab1ViewController.h"
#import "RestKit/RestKit.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"
#import "Constants.h"
#import "RestKit/RKRequestSerialization.h"

@interface BreastIntraEditTab1ViewController ()
{
	int which_tag_date_is_being_updated;
}

@end

@implementation BreastIntraEditTab1ViewController

@synthesize SecondSurgicalProcedureTextField;
@synthesize SecondSiteOfOperationSegmentedControl;
@synthesize SecondDateOfSurgeryTextField;
@synthesize SecondSurgeryWeight;
@synthesize DateOfSurgeryTextField;
@synthesize SiteOfOperationSegmentedControl;
@synthesize SurgicalProcedureTextField;
@synthesize FirstSurgeryWeight;
@synthesize RequestModificationAccessButton;
@synthesize SelectFirstSurgicalTextButton;
@synthesize SelectFirstSurgeryDateButton;
@synthesize SelectSecondSurgicalTextButton;
@synthesize SelectSecondSurgicalDateText;
@synthesize SecondSurgicalPerformedSegmentedControl;
@synthesize SecondSurgicalPerformedView;

@synthesize pickerView;
@synthesize datePicker;
@synthesize surgicalProcedureList;
@synthesize secondSurgicalProcedureList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

- (void)initPickerArrays
{
	self.surgicalProcedureList			= [[NSArray alloc] initWithObjects:@"Not Perfomed", @"Total Mastectomy with Axillary Clearance", @"Total Mastectomy", @"Wide Local Excision with Axillary Clearance", @"Wide Local Excision", @"Total Mastectomy with Sentinel Node Biopsy", @"Axillary Dissection Only", nil];
	self.secondSurgicalProcedureList	= [[NSArray alloc] initWithObjects:@"Not Performed",@"Total Mastectomy with Axillary Clearance", @"Total Mastectomy", @"Wide Local Excision with Axillary Clearance", @"Wide Local Excision", @"Total Mastectomy with Sentinel Node Biopsy", @"Axillary Dissection Only", @"Flap Failed, Redo Reconstruction Surgery", nil];
}

/*!
 Used to check the sanity of the input
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    SelectFirstSurgicalTextButton.hidden = NO;
    SelectFirstSurgeryDateButton.hidden = NO;
    SelectSecondSurgicalDateText.hidden = NO;
    SelectSecondSurgicalTextButton.hidden = NO;
    FirstSurgeryWeight.userInteractionEnabled = YES;
    SecondSurgicalPerformedSegmentedControl.userInteractionEnabled = YES;
    if(SecondSurgicalPerformedSegmentedControl.selectedSegmentIndex == 1)
    {
        SecondSurgicalPerformedView.hidden = NO;
    }
    SecondSiteOfOperationSegmentedControl.userInteractionEnabled = YES;
    SecondSurgeryWeight.userInteractionEnabled = YES;
}

-(void) EnablePartialControls
{
    
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"surgicalProcedure"]]) {
        SelectFirstSurgicalTextButton.hidden = YES;
    }
    
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"surgeryDate"]]) {
        SelectFirstSurgeryDateButton.hidden = YES;
    }
    
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"surgeryWeight"]]) {
        FirstSurgeryWeight.userInteractionEnabled = NO;
    }
    // More Controls
}

-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_IntraObject;
    [SurgicalProcedureTextField setText:[surgicalProcedureList objectAtIndex:([((NSString *)[mydir objectForKey:@"surgicalProcedure"])intValue])]];
    [SiteOfOperationSegmentedControl setSelectedSegmentIndex:([((NSString *)[mydir objectForKey:@"operationSide"])intValue] - 1)];
    [DateOfSurgeryTextField setText:[mydir objectForKey:@"surgeryDate"]];
    [FirstSurgeryWeight setText:[mydir objectForKey:@"surgeryWeight"]];
    
    //Second Operation
    [SecondSurgicalProcedureTextField setText:[secondSurgicalProcedureList objectAtIndex:([((NSString *)[mydir objectForKey:@"secondSurgeryProcedure"])intValue])]];
    [SecondSiteOfOperationSegmentedControl setSelectedSegmentIndex:([((NSString *)[mydir objectForKey:@"secondOperationSide"])intValue] - 1)];
    [SecondDateOfSurgeryTextField setText:[mydir objectForKey:@"secondSurgeryDate"]];
    [SecondSurgeryWeight setText:[mydir objectForKey:@"secondSurgeryWeight"]];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self initPickerArrays];
	[self initDateStuff];

	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldIntraObject = [[HDMSModelStorage storageObject].Breast_IntraObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
}

- (void)viewDidUnload
{
	[self setSecondSurgicalPerformedView:nil];
	[self setSecondSurgicalPerformedSegmentedControl:nil];
	[self setSurgicalProcedureTextField:nil];
	[self setSiteOfOperationSegmentedControl:nil];
	[self setDateOfSurgeryTextField:nil];
	[self setSecondSurgicalProcedureTextField:nil];
	[self setSecondSiteOfOperationSegmentedControl:nil];
	[self setSecondDateOfSurgeryTextField:nil];
	[self setFirstSurgeryWeight:nil];
	[self setSecondSurgeryWeight:nil];
	[self setSelectFirstSurgicalTextButton:nil];
	[self setSelectFirstSurgeryDateButton:nil];
	[self setSelectSecondSurgicalTextButton:nil];
	[self setSelectSecondSurgicalDateText:nil];
	[self setRequestModificationAccessButton:nil];
	[super viewDidUnload];
	// Release any retained subviews of the main view.
}

- (void)initializeThePickerView
{
	// Must display the UIPicker
	self.pickerView						= [[UIPickerView alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
	self.pickerView.dataSource			= self;
	self.pickerView.delegate			= self;
	pickerView.showsSelectionIndicator	= YES;
	[self.view addSubview:pickerView];
	self.pickerView.hidden = YES;
}

- (UIPickerView *)constructPickerViewWithTag:(int)tagNumber
{
	UIPickerView *myPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];

	myPicker.dataSource = self;
	myPicker.delegate	= self;
	myPicker.showsSelectionIndicator = YES;
	myPicker.tag = tagNumber;
	[self.view addSubview:myPicker];
	myPicker.hidden = YES;
	return myPicker;
}

- (void)initDateStuff
{
	NSDateFormatter *df = [[NSDateFormatter alloc] init];

	df.dateStyle = NSDateFormatterMediumStyle;
	[df setDateFormat:@"yyyy-MM-dd"];
	datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
	datePicker.datePickerMode	= UIDatePickerModeDate;
	datePicker.hidden			= YES;
	datePicker.date				= [NSDate date];

	[datePicker addTarget		:self
				action			:@selector(DateChange:)
				forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:datePicker];
}

- (void)DateChange:(id)sender
{
	NSDateFormatter *df = [[NSDateFormatter alloc] init];

	//    df.dateStyle = NSDateFormatterMediumStyle;
	[df setDateFormat:@"yyyy-MM-dd"];
	NSString *myDate = [NSString stringWithFormat:@"%@",
		[df stringFromDate:datePicker.date]];
	switch (which_tag_date_is_being_updated) {
		case 101:
			// planned discharge date
			self.DateOfSurgeryTextField.text = myDate;
			break;

		case 102:	// actual
			self.SecondDateOfSurgeryTextField.text = myDate;
			break;

		default:
			break;
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}

- (IBAction)ndSurgicalPerformedAction:(id)sender
{
	if (self.SecondSurgicalPerformedSegmentedControl.selectedSegmentIndex == 0) {
		self.SecondSurgicalPerformedView.hidden = NO;
	}

	if (self.SecondSurgicalPerformedSegmentedControl.selectedSegmentIndex == 1) {
		self.SecondSurgicalPerformedView.hidden = YES;
	}
}

- (IBAction)SelectSurgicalProcedureAction:(id)sender
{
	self.pickerView			= [self constructPickerViewWithTag:1];
	self.pickerView.hidden	= NO;
}

- (IBAction)SelectDateOfSurgeryAction:(UIButton *)sender
{
	which_tag_date_is_being_updated = sender.tag;
	datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)RequestModificationAccessAction:(UIBarButtonItem *)sender 
{
    [HDMSModelStorage storageObject].FormMode = FORM_MODE_EDIT;
    [self viewDidLoad];
}

- (IBAction)SecondSelectSurgicalProcedureAction:(id)sender
{
	self.pickerView			= [self constructPickerViewWithTag:2];
	self.pickerView.hidden	= NO;
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
   (UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)	pickerView				:(UIPickerView *)thePickerView
				numberOfRowsInComponent :(NSInteger)component
{
	NSInteger pickerViewTag = thePickerView.tag;

	switch (pickerViewTag) {
		case 1:
			// For Comorbid
			return [self.surgicalProcedureList count];

			break;

		case 2:	// For ASA Score
			return [self.secondSurgicalProcedureList count];

			break;

		default:
			break;
	}
	return 0;
}

- (NSString *)	pickerView	:(UIPickerView *)thePickerView
				titleForRow :(NSInteger)row
				forComponent:(NSInteger)component
{
	NSInteger pickerViewTag = thePickerView.tag;

	switch (pickerViewTag) {
		case 1:
			return [self.surgicalProcedureList objectAtIndex:row];

			break;

		case 2:
			return [self.secondSurgicalProcedureList objectAtIndex:row];

			break;

		default:
			break;
	}
	return @"NONE";
}

#pragma mark PickerView Delegate
- (void)pickerView	:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
		inComponent :(NSInteger)component
{
	NSInteger pickerViewTag = thePickerView.tag;

	switch (pickerViewTag) {
		case 1:
			// Surgical Procedure List
			self.SurgicalProcedureTextField.text = [self.surgicalProcedureList objectAtIndex:row];
			break;

		case 2:
			// Second surgical procedure list
			self.SecondSurgicalProcedureTextField.text = [self.secondSurgicalProcedureList objectAtIndex:row];
			break;

		default:
			break;
	}
	thePickerView.hidden = YES;
}

- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Intra Object
	[[HDMSModelStorage storageObject].Breast_IntraObject setObject:[self sanitizeValue:value] forKey:key];
}

- (NSString *)GetIndexForSurgicalProcedureString:(NSString *)indexText
{
	int index = [self.surgicalProcedureList indexOfObject:indexText];

	if ((index > 0) && (index < surgicalProcedureList.count)) {
		return [NSString stringWithFormat:@"%i", index];
	}

	return @"";
}

- (NSString *)GetIndexForSecondSurgicalProcedureString:(NSString *)indexText
{
	int index = [self.secondSurgicalProcedureList indexOfObject:indexText];

	if ((index > 0) && (index < secondSurgicalProcedureList.count)) {
		return [NSString stringWithFormat:@"%i", index];
	}

	return @"";
}

- (void)saveValues
{
	NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];

	NSString	*surgicalProcedure			= [self GetIndexForSurgicalProcedureString:SurgicalProcedureTextField.text];
	NSString	*siteOfOperation			= [NSString stringWithFormat:@"%i", (self.SiteOfOperationSegmentedControl.selectedSegmentIndex + 1)];
	NSString	*dateOfSurgery				= DateOfSurgeryTextField.text;
	NSString	*secondSurgicalProcedure	= [self GetIndexForSecondSurgicalProcedureString:SecondSurgicalProcedureTextField.text];
	NSString	*firstSurgeryWeight			= FirstSurgeryWeight.text;
	NSString	*secondSiteOfOperation		= [NSString stringWithFormat:@"%i", (self.SecondSiteOfOperationSegmentedControl.selectedSegmentIndex + 1)];
	NSString	*secondDateOfSurgery		= SecondDateOfSurgeryTextField.text;
	NSString	*secondSurgeryWeight		= SecondSurgeryWeight.text;

	// Store into the dictionary
	[self SanitizeAndStoreValue:formID forKey:@"formID"];
	[self SanitizeAndStoreValue:surgicalProcedure forKey:@"surgicalProcedure"];
	[self SanitizeAndStoreValue:siteOfOperation forKey:@"operationSide"];
	[self SanitizeAndStoreValue:dateOfSurgery forKey:@"surgeryDate"];
	[self SanitizeAndStoreValue:firstSurgeryWeight forKey:@"surgeryWeight"];
	[self SanitizeAndStoreValue:secondSurgicalProcedure forKey:@"secondSurgeryProcedure"];
	[self SanitizeAndStoreValue:secondSiteOfOperation forKey:@"secondOperationSide"];
	[self SanitizeAndStoreValue:secondDateOfSurgery forKey:@"secondSurgeryDate"];
	[self SanitizeAndStoreValue:secondSurgeryWeight forKey:@"secondSurgeryWeight"];

	/*NSArray* values = [[NSArray alloc]initWithObjects: formID, surgicalProcedure, siteOfOperation, dateOfSurgery, secondSurgicalProcedure, secondSurgicalProcedure, secondDateOfSurgery, nil];
	 *   NSArray* keys = [[NSArray alloc]initWithObjects:@"formID", @"surgicalProcedure", @"operationSide",@"surgeryDate",@"2ndSurgeryProcedure", @"2ndOperationSide", @"2ndSurgeryDate", nil];
	 */

	/*NSDictionary *params = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
	 *   id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
	 *   NSError			*error	= nil;
	 *   NSString		*json	= [parser stringFromObject:params error:&error];
	 *
	 *   [[RKClient sharedClient] post:[Constants BreastIntraAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];*/
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[CommonUtils DisplayModalPopupWithMessage:@"Request Succeeded"];
			[CommonUtils StandardDebugLogWithString:[response bodyAsString]];
		} else {
			[CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat:@"Request Did not Succeed %@", [response bodyAsString]]];
		}
	} else if ([request isGET]) {
		// Process the json here
		NSString *json = [response bodyAsString];

		if ((json == nil) || (json == NULL) || [json isEqualToString:@"null"]) {
			// Nothing its fine
			return;
		} else {
			id <RKParser>	parser		= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
			NSError			*error		= nil;
			NSDictionary	*results	= [parser objectFromString:json error:&error];

			[CommonUtils StandardDebugLogWithString:[NSString stringWithFormat:@"Response %@", [response bodyAsString]]];

			for (NSString *key in results) {
				NSString *value = [results objectForKey:key];
				[[HDMSModelStorage storageObject].Breast_IntraObject setObject:[self sanitizeValue:value] forKey:key];
			}
		}

		if (([response statusCode] == 404) || ([response statusCode] == 400)) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_NEW;
			[self viewDidLoad];
		}

		if (([response statusCode] == 201) && [[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"2"]) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_PARTIAL_EDIT;
			[self viewDidLoad];
		} else if (([response statusCode] == 201) && [[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"1"]) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_EDIT;
			[self viewDidLoad];
		}
	}
}

- (IBAction)NavigateToSecondTab:(UIBarButtonItem *)sender
{
	[self saveValues];
	[self performSegueWithIdentifier:@"FirstTabToSecondTab" sender:self];
}

@end
//
//  BreastReconTableViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 31/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastReconTableViewController.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"

@interface BreastReconTableViewController ()

@end

@implementation BreastReconTableViewController

@synthesize BreastReconIntraRKRequestDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)goBacktoHome:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    //HomeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
    HomeViewController* hvc = [storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
    // UINavigationController *kkc = [hvc.viewControllers objectAtIndex:0];
    //UIViewController* mt = [hvc.viewControllers objectAtIndex:1];
    //self.splitViewController.viewControllers = [[NSArray alloc]initWithObjects:kkc, mt,nil];
    self.splitViewController.viewControllers  = hvc.viewControllers;
}

#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    // Configure the cell...
//    
//    return cell;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
	int rowSelected = indexPath.row;
	// int sectionSelected = indexPath.section;    
	switch (rowSelected) {
		case 0:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BreastReconPreStoryBoard" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FirstView"];
            UIViewController *masterView = [self.splitViewController.viewControllers objectAtIndex:0];
            NSArray *mArr2 = [[NSArray alloc] initWithObjects:masterView, vc, nil];
            self.splitViewController.viewControllers = mArr2;
            
            UINavigationController* v1 = [self.splitViewController.viewControllers objectAtIndex:1];
            
            NSString* formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
            [[RKClient sharedClient] get: [Constants BreastPreGetURLByFormID:formID] delegate:[v1.viewControllers objectAtIndex:0]];
            // The delegate is handled by the first view controller of the intra view
            [HDMSModelStorage storageObject].FormMode = FORM_MODE_DEFAULT;
        }
			break;
        case 1: 
        {
            // Initializing the breast intra storyboard
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BreastReconIntraStoryBoard" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BreastReconIntraFirstView"];
            UIViewController *masterView = [self.splitViewController.viewControllers objectAtIndex:0];
            NSArray *mArr2 = [[NSArray alloc] initWithObjects:masterView, vc, nil];
            self.splitViewController.viewControllers = mArr2;
            
            UINavigationController* v1 = [self.splitViewController.viewControllers objectAtIndex:1];
            
            NSString* formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
            [[RKClient sharedClient] get: [Constants BreastIntraGetURLByFormID:formID] delegate:[v1.viewControllers objectAtIndex:0]];
            // The delegate is handled by the first view controller of the intra view
            [HDMSModelStorage storageObject].FormMode = FORM_MODE_DEFAULT;
        } break;
        case 2:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BreastReconPostStoryBoard" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FirstViewBreastReconPost"];
            
            UIViewController *masterView = [self.splitViewController.viewControllers objectAtIndex:0];
            [masterView.navigationController pushViewController:vc animated:YES];
            NSArray		*mArr	= self.splitViewController.viewControllers;
           /* NSArray		*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];*/
            NSArray *mArr2 = [[NSArray alloc] initWithObjects:masterView, [mArr objectAtIndex:1] , nil];
            self.splitViewController.viewControllers = mArr2;
            [[self.splitViewController.viewControllers objectAtIndex:0] pushViewController:vc animated:YES];
        }
			break;
        
    }

}

@end

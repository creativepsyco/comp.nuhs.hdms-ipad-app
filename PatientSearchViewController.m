//
//  PatientSearchViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PatientSearchViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "RestKit/JSONKit.h"
#import "Constants.h"
#import "PatientHistoryViewController.h"
#import "HDMSModelStorage.h"
#import "AddFormViewController.h"

@interface PatientSearchViewController ()
{
	// Strictly Patient Objects of the type
	// identification
	// name
	NSArray *objects;
	Boolean NoResultsToDisplay;
}
@end

@implementation PatientSearchViewController
@synthesize resultsTableView;
@synthesize SearchKeyword;
@synthesize patientMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.resultsTableView.dataSource	= self;
	self.resultsTableView.delegate		= self;
	objects				= [[NSArray alloc] init];
	NoResultsToDisplay	= YES;
	// objects = [NSArray arrayWithObjects:@"Mohit", @"Nayan", nil];

	// Uncomment the following line to preserve selection between presentations.
	// self.clearsSelectionOnViewWillAppear = NO;

	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	// self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
	[self setSearchKeyword:nil];
	[self setResultsTableView:nil];
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	if (NoResultsToDisplay) {
		return 1;
	}

	return [objects count];
}

/* For setting the individual cell properties in case of a redraw */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell			= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	// Configure the cell...
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}

	if (NoResultsToDisplay && (indexPath.row == 0)) {
		cell.textLabel.text = @"No Results";
	}

	if (NoResultsToDisplay == NO) {
		NSDictionary *patientObject = [objects objectAtIndex:indexPath.row];
		cell.textLabel.text			= [patientObject objectForKey:@"name"];
		cell.detailTextLabel.text	= [patientObject objectForKey:@"identification"];
		cell.imageView.image		= [UIImage imageNamed:@"presence_offline.png"];
		cell.accessoryType			= UITableViewCellAccessoryDetailDisclosureButton;
	}

	return cell;
}

/*
 *   // Override to support conditional editing of the table view.
 *   - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    // Return NO if you do not want the specified item to be editable.
 *    return YES;
 *   }
 */

/*
 *   // Override to support editing the table view.
 *   - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    if (editingStyle == UITableViewCellEditingStyleDelete) {
 *        // Delete the row from the data source
 *        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 *    }
 *    else if (editingStyle == UITableViewCellEditingStyleInsert) {
 *        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 *    }
 *   }
 */

/*
 *   // Override to support rearranging the table view.
 *   - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 *   {
 *   }
 */

/*
 *   // Override to support conditional rearranging of the table view.
 *   - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 *   {
 *    // Return NO if you do not want the item to be re-orderable.
 *    return YES;
 *   }
 */

#pragma mark - Table view delegate

// - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
// {
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
// }

- (IBAction)BeginSearch:(id)sender
{
	[[RKClient sharedClient] get:[Constants GetPatientListByURLKeyword: SearchKeyword.text] delegate:self];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isGET] && ([response statusCode] == 201)) {
		NSString *json = [response bodyAsString];

		if ((json == nil) || (json == NULL) || [json isEqualToString:@"null"]) {
			NoResultsToDisplay	= YES;
			objects				= nil;
			[resultsTableView reloadData];
			return;
		}

		NoResultsToDisplay = NO;
		id <RKParser>	parser		= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
		NSError			*error		= nil;
		NSDictionary	*results	= [parser objectFromString:json error:&error];

		NSLog(@"Response %@", [response bodyAsString]);
		[self showJsonSearchResults:results];
	}
}

-(void) AddSelectedPatientInsideModel
{
    int selected_row = [self.resultsTableView indexPathForSelectedRow].row;
    NSDictionary* patientDict = [objects objectAtIndex:selected_row];
    [HDMSModelStorage storageObject].PatientObject = [patientDict copy];
}

- (void)showJsonSearchResults:(NSDictionary *)results
{
	NSMutableArray *myObjects = [[NSMutableArray alloc] init];
	// store into the object table
	NSObject *patientsResultObject = [results objectForKey:@"patient"];

	if ([patientsResultObject respondsToSelector:@selector(objectAtIndex:)]) {
		// Its an array
		NSArray *patientsResult = [results objectForKey:@"patient"];

		for (NSDictionary *patientRecord in patientsResult) {
			[myObjects addObject:[patientRecord objectForKey:@"name"]];
		}

		objects = [NSArray arrayWithArray:[patientsResult copy]];
	} else {
		// Its a single patient object
		NSDictionary *patientRecord = [results objectForKey:@"patient"];
		objects = [NSArray arrayWithObject:patientRecord];
	}

	// objects = [NSArray arrayWithArray: [myObjects copy]] ;

	[resultsTableView reloadData];
	//    [self.resultsTableView indexPathForSelectedRow];
}

- (BOOL)DidSelectTheCorrectPatient
{
	NSString *str = [NSString stringWithFormat: @"selected row: %i section: %i", [self.resultsTableView indexPathForSelectedRow].row, [self.resultsTableView indexPathForSelectedRow].section];
    [CommonUtils StandardDebugLogWithString:str];

	if ([objects count] > 0) {
//		int selected_row = [self.resultsTableView indexPathForSelectedRow].row;
        [self AddSelectedPatientInsideModel];
		return YES;
	}

	return NO;
}

- (IBAction)UpdateRecord:(id)sender
{
	if ([self DidSelectTheCorrectPatient]) {
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];

		UpdatePatientViewController *vc		= [storyboard instantiateViewControllerWithIdentifier:@"UpdatePatientView"];
		NSArray						*mArr	= self.splitViewController.viewControllers;
		NSArray						*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];

		self.splitViewController.viewControllers = mArr2;
	} else {
		[CommonUtils DisplayModalPopupWithMessage:@"Please select a patient first"];
	}
}

- (IBAction)UpdateRecordAction:(id)sender {}

- (IBAction)ViewPatientHistoryAction:(id)sender
{
	if([self DidSelectTheCorrectPatient]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        
        PatientHistoryViewController	*vc		= [storyboard instantiateViewControllerWithIdentifier:@"PatientHistoryView"];
        NSArray							*mArr	= self.splitViewController.viewControllers;
        NSArray							*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
        
        self.splitViewController.viewControllers = mArr2;
    }
    else {
        [CommonUtils DisplayModalPopupWithMessage:@"Please select a patient first"];
    }
}

- (IBAction)ViewThisPatientDetailsAction:(id)sender {}

- (IBAction)AddRecordAction:(id)sender {
    if([self DidSelectTheCorrectPatient])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        AddFormViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddFormView"]; 
        NSArray							*mArr	= self.splitViewController.viewControllers;
        NSArray							*mArr2	= [[NSArray alloc] initWithObjects:[mArr objectAtIndex:0], vc, nil];
        
        self.splitViewController.viewControllers = mArr2;
        
    } else {
        [CommonUtils DisplayModalPopupWithMessage:@"Please select a patient first"];
    }
}

@end
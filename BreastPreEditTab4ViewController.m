//
//  BreastPreEditTab4ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreEditTab4ViewController.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"

@interface BreastPreEditTab4ViewController ()
{
    int current_item_change_id; // Describes the tag property of the item being changed
}
@end

@implementation BreastPreEditTab4ViewController
@synthesize IDCGrade;
@synthesize DCISGrade;
@synthesize ExternalDCISGrade;
@synthesize InfiltrativeDuctalCarcinoma;
@synthesize MucinousCarcinoma;
@synthesize MasectomyOtherReason;
@synthesize FibroadenomataPresence;
@synthesize FatNecrosisPresence;
@synthesize SacromaPresence;
@synthesize RecurrentBreastCarcinomaPresence;
@synthesize pickerView, datePicker, ASAScoresList, ComorbidConditionsList, GradesList; //!Pickers

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self initPickerArrays];
	[self initDateStuff];
    
	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//!Picker View Stuff

// Stuff that is loaded just after start comes here

-(void) initPickerArrays
{
    self.ComorbidConditionsList = [[NSArray alloc]initWithObjects:@"COPD", @"DM Type I", @"CHF/CCF", @"CVA", @"IHD", @"Hypertension", @"Others", nil];
    self.ASAScoresList = [[NSArray alloc]initWithObjects:@"ASA I", @"ASA II", @"ASA III", @"ASA IV", @"ASA V", nil];
    self.GradesList = [[NSArray alloc] initWithObjects:@"Grade 1", @"Grade 2", @"Grade 3", nil];
}



- (void)initializeThePickerView {
    // Must display the UIPicker
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    self.pickerView.hidden = YES;
}

- (UIPickerView*) constructPickerViewWithTag: (int) tagNumber
{
    UIPickerView *myPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    myPicker.dataSource = self;
    myPicker.delegate = self;
    myPicker.showsSelectionIndicator = YES;
    myPicker.tag = tagNumber;
    [self.view addSubview:myPicker];
    myPicker.hidden = YES;
    return myPicker;
}

- (void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];      
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(DateChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

// is referred to by DatePicker on completion
- (void)DateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    NSString *myDate = [NSString stringWithFormat:@"%@",
                        [df stringFromDate:datePicker.date]];
    switch (current_item_change_id) {
            // Do stuff relevant to changing date of different label
        default:
            break;
    }
}



#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the stuff relevant to the picker view pressed 
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //For Comorbid
            return [self.ComorbidConditionsList count];
            break;
        case 104: //For ASA Score
            return [self.ASAScoresList count];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList count]; break;
        default:
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            return [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            return [self.ASAScoresList objectAtIndex:row];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    return @"NONE";
} 

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
            // Tab 4
        case 401: IDCGrade.text = [self.GradesList objectAtIndex:row]; break;
        case 402: DCISGrade.text = [self.GradesList objectAtIndex:row]; break;        
        case 403: ExternalDCISGrade.text = [self.GradesList objectAtIndex:row]; break;
        case 404: InfiltrativeDuctalCarcinoma.text = [self.GradesList objectAtIndex:row]; break;
        case 405: MucinousCarcinoma.text = [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    thePickerView.hidden = YES;
}


- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)Tab4GradeSelected:(UISegmentedControl *)sender {
    current_item_change_id = sender.tag;
    if(sender.selectedSegmentIndex == 1){
        self.pickerView = [self constructPickerViewWithTag:sender.tag];
        self.pickerView.hidden = NO;
    }
}

- (IBAction)NavigateToTab5:(UIBarButtonItem *)sender {
    [self SaveDataIntoModel];
    [self performSegueWithIdentifier:@"FromTab4ToTab5" sender:self];
}

/// UI Control Management Region
/*!
 Used to check the sanity of the input
 Returns True if string is not empty
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}
/*! Returns the grade depending on the index
 */
- (NSString*) GetGradeFromIndex: (NSString*) index
{
    NSInteger val = [index intValue]-1;
    if(val>=0 && val <[GradesList count])
    {
        return [GradesList objectAtIndex:val];
    }
    else return @"";
}

/*! Returns the index depending on the grade
 */
- (NSString*) GetIndexFromGrade: (NSString*) grade
{
    NSInteger index = [GradesList indexOfObject:grade]+1 ;
    if(index < [GradesList count])
        return [NSString stringWithFormat:@"%i", index];
    else {
        return @"";
    }
}

/*!
 * Loads the dictionary data into view
 */
-(void) LoadDataIntoView
{
    // Must set the segmented controls as well
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    if(![[self GetGradeFromIndex:[mydir objectForKey:@"IDC"]] isEqualToString:@""])
        [IDCGrade setText:[self GetGradeFromIndex:[mydir objectForKey:@"IDC"]]];

    if(![[self GetGradeFromIndex:[mydir objectForKey:@"DCIS"]] isEqualToString:@""])
        [DCISGrade setText:[self GetGradeFromIndex:[mydir objectForKey:@"DCIS"]]];

    if(![[self GetGradeFromIndex:[mydir objectForKey:@"EDCinSitu"]] isEqualToString:@""])
        [ExternalDCISGrade setText:[self GetGradeFromIndex:[mydir objectForKey:@"EDCinSitu"]]];

    if(![[self GetGradeFromIndex:[mydir objectForKey:@"ifDC"]] isEqualToString:@""])
        [InfiltrativeDuctalCarcinoma setText:[self GetGradeFromIndex:[mydir objectForKey:@"ifDC"]]];

    if(![[self GetGradeFromIndex:[mydir objectForKey:@"MC"]] isEqualToString:@""])
        [MucinousCarcinoma setText:[self GetGradeFromIndex:[mydir objectForKey:@"MC"]]];
    [FibroadenomataPresence setSelectedSegmentIndex:[[mydir objectForKey:@"fibroadennomata"] intValue]];
    [FatNecrosisPresence setSelectedSegmentIndex:[[mydir objectForKey:@"fatNecrosis"] intValue]];
    [SacromaPresence setSelectedSegmentIndex:[[mydir objectForKey:@"sarcoma"] intValue]];
    [RecurrentBreastCarcinomaPresence setSelectedSegmentIndex: [[mydir objectForKey:@"RBC"] intValue]];
    [MasectomyOtherReason setText:[mydir objectForKey:@"otherForMastectomy"]];

}
/**!
 * For sanitizing empty values
 */
- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Pre Object
	[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
    
}
/*!
 * Save Data into Model
 */
-(void) SaveDataIntoModel
{
    NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    NSString *idc = [self GetIndexFromGrade:IDCGrade.text];
    NSString *dcis = [self GetIndexFromGrade:DCISGrade.text];
    NSString *edcis = [self GetIndexFromGrade:ExternalDCISGrade.text];
    NSString *ifdcs = [self GetIndexFromGrade:InfiltrativeDuctalCarcinoma.text];
    NSString *mc = [self GetIndexFromGrade:MucinousCarcinoma.text];    
    NSString *fib = [NSString stringWithFormat:@"%i",[FibroadenomataPresence selectedSegmentIndex]];
    NSString *fat = [NSString stringWithFormat:@"%i",[FatNecrosisPresence selectedSegmentIndex]];
    NSString *sarcoma = [NSString stringWithFormat:@"%i",[SacromaPresence selectedSegmentIndex]];
    NSString *rbc = [NSString stringWithFormat:@"%i",[RecurrentBreastCarcinomaPresence selectedSegmentIndex]];
    
    [self SanitizeAndStoreValue:formID forKey:@"formID"];
    [self SanitizeAndStoreValue:idc forKey:@"IDC"];
    [self SanitizeAndStoreValue:dcis forKey:@"DCIS"];
    [self SanitizeAndStoreValue:edcis forKey:@"EDCinSitu"];
    [self SanitizeAndStoreValue:ifdcs forKey:@"ifDC"];
    [self SanitizeAndStoreValue:mc forKey:@"MC"];
    [self SanitizeAndStoreValue:fib forKey:@"fibroadennomata"];
    [self SanitizeAndStoreValue:fat forKey:@"fatNecrosis"];
    [self SanitizeAndStoreValue:sarcoma forKey:@"sarcoma"];
    [self SanitizeAndStoreValue:rbc forKey:@"RBC"];
    [self SanitizeAndStoreValue:MasectomyOtherReason.text forKey:@"otherForMastectomy"];
    
    NSLog([[HDMSModelStorage storageObject].Breast_PreObject description]);
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    //Don't touch for now
}

-(void) EnablePartialControls
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    //! Partial control
}


@end

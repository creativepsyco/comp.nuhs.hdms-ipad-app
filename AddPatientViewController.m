//
//  AddPatientViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddPatientViewController.h"
#import "RestKit/RestKit.h"
#import "Constants.h"
#import "RestKit/RKRequestSerialization.h"
#import "Restkit/JSONKit.h"

@interface AddPatientViewController ()

@end

@implementation AddPatientViewController
@synthesize PatientID;
@synthesize Gender;
@synthesize Ethnicity;
@synthesize Weight;
@synthesize Occupation;
@synthesize Address;
@synthesize MotherID;
@synthesize Name;
@synthesize Birthdate;
@synthesize Height;
@synthesize Smoker;
@synthesize HomeNumber;
@synthesize MobileNumber;
@synthesize OfficeNumber;
@synthesize FatherID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
	[self setPatientID:nil];
	[self setGender:nil];
	[self setEthnicity:nil];
	[self setWeight:nil];
	[self setOccupation:nil];
	[self setAddress:nil];
	[self setMotherID:nil];
	[self setName:nil];
	[self setBirthdate:nil];
	[self setHeight:nil];
	[self setSmoker:nil];
	[self setHomeNumber:nil];
	[self setMobileNumber:nil];
	[self setOfficeNumber:nil];
	[self setFatherID:nil];
	[super viewDidUnload];
	// Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

// Make a request to the web
- (IBAction)AddPatientAction:(id)sender
{
	NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:self.PatientID.text, @"identification", self.Name.text, @"name",
		self.Gender.text, @"gender",
		self.Birthdate.text, @"birthday",
		self.Ethnicity.text, @"ethnic",
		self.Height.text, @"height",
		self.Weight.text, @"weight",
		self.Smoker.text, @"smoker",
		self.Occupation.text, @"occupation",
		self.Address.text, @"address",
		self.HomeNumber.text, @"homeNumber",
		self.OfficeNumber.text, @"officeNumber",
		self.MobileNumber.text, @"mobileNumber",
		self.MotherID.text, @"motherIdentification",
		self.FatherID.text, @"fatherIdentification",
		nil];

	id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
	NSError			*error	= nil;
	NSString		*json	= [parser stringFromObject:params error:&error];

	[[RKClient sharedClient] post:[Constants PatientAddURL] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];

	NSLog(@"JsonString %@", [params JSONString]);
}

- (void)DisplayModalPopup:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
		@"HDMS App"
        message			:
		message
		delegate
		:self cancelButtonTitle:
		@"Close"
		otherButtonTitles:nil];

	[alert show];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	NSLog(@"Response Body%@", [response bodyAsString]);

	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[self DisplayModalPopup:@"Request Carried out successfully"];
            [self ClearAllFieldsAction:self];
		}
	}
}

- (IBAction)ClearAllFieldsAction:(id)sender
{
	[self setPatientID:nil];
	[self setGender:nil];
	[self setEthnicity:nil];
	[self setWeight:nil];
	[self setOccupation:nil];
	[self setAddress:nil];
	[self setMotherID:nil];
	[self setName:nil];
	[self setBirthdate:nil];
	[self setHeight:nil];
	[self setSmoker:nil];
	[self setHomeNumber:nil];
	[self setMobileNumber:nil];
	[self setOfficeNumber:nil];
	[self setFatherID:nil];
}

@end
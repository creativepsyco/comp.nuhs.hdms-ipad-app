//
//  PatientSearchViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restkit/RestKit.h"
#import "ManagePatientMenuViewController.h"
#import "CommonUtils.h"
#import "UpdatePatientViewController.h"

@interface PatientSearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *SearchKeyword;
- (IBAction)BeginSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;

@property (weak, nonatomic) ManagePatientMenuViewController *patientMenu;

// Update Patient Record
- (IBAction)UpdateRecord:(id)sender;
// View the selected patient's history
- (IBAction)ViewPatientHistoryAction:(id)sender;
// View this Patient's Details
- (IBAction)ViewThisPatientDetailsAction:(id)sender;
// AddADiseaseRecord
- (IBAction)AddRecordAction:(id)sender;

@end

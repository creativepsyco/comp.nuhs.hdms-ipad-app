//
//  AddFormViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "HDMSModelStorage.h"

@interface AddFormViewController : UIViewController <RKRequestDelegate>
{
    
IBOutlet UIDatePicker *datePicker;
IBOutlet UILabel *datelabel;
}

@property(nonatomic,retain) UIDatePicker *datePicker;
@property(nonatomic,retain) IBOutlet UILabel *datelabel;
@property (weak, nonatomic) IBOutlet UITextField *PatientID;
@property (weak, nonatomic) IBOutlet UITextField *PatientName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *DiseaseType;
@property (weak, nonatomic) IBOutlet UILabel *SelectedDate;
- (IBAction)ShowSelectDatePopOver:(id)sender;
- (IBAction)AddFormAction:(id)sender;

@end

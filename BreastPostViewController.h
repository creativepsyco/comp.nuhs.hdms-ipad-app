//
//  BreastPostViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreastPostViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *FirstLabel;
@property (weak, nonatomic) IBOutlet UILabel *SecondLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThirdPage;

@property (nonatomic, retain) NSArray* postComplicationsList;

@end

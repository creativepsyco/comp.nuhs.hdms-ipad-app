//
//  HDMSModelStorage.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDMSUser.h"
#import "CommonUtils.h"
#import "HDMSAppConfig.h"
#import "OfflineDataAccess.h"
// This class stores all the models that are needed for instantiation
// This is a singleton class

@interface HDMSModelStorage : NSObject
@property (nonatomic, retain) HDMSUser* UserObject;
@property (nonatomic, retain) HDMSAppConfig* Config;
@property (nonatomic, retain) OfflineDataAccess* OfflineDAO;
@property (nonatomic, retain) NSDictionary* PatientObject;
@property (nonatomic, retain) NSDictionary* FormObject; // Contains, disease, patient,
@property (nonatomic, retain) NSMutableDictionary* Disease_PreObject;
@property (nonatomic, retain) NSMutableDictionary* Disease_IntraObject;
@property (nonatomic, retain) NSMutableDictionary* Disease_PostObject;

@property (nonatomic, retain) NSMutableDictionary* Breast_PreObject;
@property (nonatomic, retain) NSMutableDictionary* Breast_IntraObject;
@property (nonatomic, retain) NSMutableDictionary* Breast_PostObject;
@property (nonatomic, assign) FORM_MODE FormMode;

/*! For Storing Old Form Information*/
@property (nonatomic, retain) NSMutableDictionary* Breast_OldPreObject;
@property (nonatomic, retain) NSMutableDictionary* Breast_OldIntraObject;
@property (nonatomic, retain) NSMutableDictionary* Breast_OldPostObject;

/*! For storing the difference */
@property (nonatomic, retain) NSMutableDictionary* Diff_Object;
+ (HDMSModelStorage*) storageObject ;

// For the offline Access and Storage
@property (nonatomic, retain) NSMutableDictionary *libraryContent;
- (void)saveOffline;
@end

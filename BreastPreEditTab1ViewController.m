//
//  BreastPreEditTab1ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 15/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreEditTab1ViewController.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKRequestSerialization.h"
#import "CommonUtils.h"
#import "HDMSModelStorage.h"

@interface BreastPreEditTab1ViewController ()
{
    int current_item_change_id; // Describes the tag property of the item being changed
}

@end

@implementation BreastPreEditTab1ViewController
@synthesize RequestModificationAccessButton;
@synthesize SelectDischargeDateButton;
@synthesize SelectActualDischargeDateButton;
@synthesize SelectASAScoreButton;
@synthesize ReconstructionAge;
@synthesize PlannedDischargeDate;
@synthesize ActualDischargeDate;
@synthesize BraSize;
@synthesize BMI;
@synthesize ComorbidCondition;
@synthesize ComorbidConditionType;
@synthesize ComorbidConditionTypeOthers;
@synthesize ASAScore;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
	[self initPickerArrays];
	[self initDateStuff];
    
	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }

}

- (void)viewDidUnload
{
    [self setSelectDischargeDateButton:nil];
    [self setSelectActualDischargeDateButton:nil];
    [self setSelectASAScoreButton:nil];
    [self setRequestModificationAccessButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

/*!
 Picker View and Date Selection Code
 */

@synthesize pickerView, datePicker, ASAScoresList, ComorbidConditionsList, GradesList;

-(void) initPickerArrays
{
    self.ComorbidConditionsList = [[NSArray alloc]initWithObjects:@"COPD", @"DM Type I", @"CHF/CCF", @"CVA", @"IHD", @"Hypertension", @"Others", nil];
    self.ASAScoresList = [[NSArray alloc]initWithObjects:@"ASA I", @"ASA II", @"ASA III", @"ASA IV", @"ASA V", nil];
    self.GradesList = [[NSArray alloc] initWithObjects:@"Grade 1", @"Grade 2", @"Grade 3", nil];
}

- (void)initializeThePickerView {
    // Must display the UIPicker
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    self.pickerView.hidden = YES;
}

- (UIPickerView*) constructPickerViewWithTag: (int) tagNumber
{
    UIPickerView *myPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(200, 250, 325, 300)];
    myPicker.dataSource = self;
    myPicker.delegate = self;
    myPicker.showsSelectionIndicator = YES;
    myPicker.tag = tagNumber;
    [self.view addSubview:myPicker];
    myPicker.hidden = YES;
    return myPicker;
}

- (void) initDateStuff 
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];      
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(200, 250, 325, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = YES;
    datePicker.date = [NSDate date];
    
    [datePicker addTarget:self
                   action:@selector(DateChange:)
         forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
}

/*!
 * Override to refer to the particular field based on the tag
 * is referred to by DatePicker on completion
 */
- (void)DateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"]; 
    NSString *myDate = [NSString stringWithFormat:@"%@",
                        [df stringFromDate:datePicker.date]];
    switch (current_item_change_id) {
            // Do stuff relevant to changing date of different labels
        case 101:
            //planned discharge date
            PlannedDischargeDate.text = myDate;
            break;
        case 102: // actual 
            ActualDischargeDate.text = myDate;
            break;
        default:
            break;
    }
}
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the stuff relevant to the picker view pressed 
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //For Comorbid
            return [self.ComorbidConditionsList count];
            break;
        case 104: //For ASA Score
            return [self.ASAScoresList count];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList count]; break;
        default:
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            return [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            return [self.ASAScoresList objectAtIndex:row];
            break;
        case 401: case 402: case 403: case 404: case 405:
            return [self.GradesList objectAtIndex:row]; break;
        default:
            break;
    }
    return @"NONE";
} 

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSInteger pickerViewTag = thePickerView.tag;
    switch (pickerViewTag) {
        case 103:
            //Comorbid
            self.ComorbidConditionType.text = [self.ComorbidConditionsList objectAtIndex:row];
            break;
        case 104:
            //ASA
            self.ASAScore.text = [self.ASAScoresList objectAtIndex:row];
            break;
            // Tab 4
        default:
            break;
    }
    thePickerView.hidden = YES;
}


- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

- (IBAction)SelectASAScore:(UIButton*)sender {
    current_item_change_id = sender.tag;
    pickerView = [self constructPickerViewWithTag:current_item_change_id];
    pickerView.hidden = (!pickerView.hidden);
}

- (IBAction)ComorbidConditionChanged:(UISegmentedControl *)sender {
    //Display a UIPicker
    current_item_change_id = sender.tag;
    if(self.ComorbidCondition.selectedSegmentIndex == 0){
        self.pickerView = [self constructPickerViewWithTag:sender.tag];
        self.pickerView.hidden = NO;
    }
}

- (IBAction)NavigateToTab2:(UIBarButtonItem *)sender {
    [self SaveDataIntoModel];
    [self performSegueWithIdentifier:@"FromTab1ToTab2" sender:self];
}

- (IBAction)RequestModificationAccess:(UIBarButtonItem *)sender {
    [HDMSModelStorage storageObject].FormMode = FORM_MODE_EDIT;
    [self viewDidLoad];
}

/*!
 End of the picker view and date code
 */

/// UI Control Management Region
/*!
 Used to check the sanity of the input
 Returns True if string is not empty
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}
/*!
 * Loads the dictionary data into view
*/
-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    [ReconstructionAge setText:[mydir objectForKey:@"reconstructionAge"]];
    [PlannedDischargeDate setText:[mydir objectForKey:@"plannedDischargeDate"]];
    [ActualDischargeDate setText:[mydir objectForKey:@"actualDischargedDate"]];
    [BMI setText: [mydir objectForKey:@"BMI"]];
    [BraSize setText:[mydir objectForKey:@"braSize"]];
    NSString* comorbid_condition = [mydir objectForKey:@"comorbidCondition"];
    
    if ([comorbid_condition isEqualToString:@"true"] || [comorbid_condition isEqualToString:@"1"])
    {
        [ComorbidCondition setSelectedSegmentIndex:0];
        NSString* condition_type = [mydir objectForKey:@"comorbidConditionType"];
        NSInteger index_of_condition = [condition_type integerValue];
        if(index_of_condition < [ComorbidConditionsList count])
            [ComorbidConditionType setText:[ComorbidConditionsList objectAtIndex:index_of_condition]];
        
        [ComorbidConditionTypeOthers setText:[mydir objectForKey:@"comorbidConditionOthers"]];
    } else {
        [ComorbidCondition setSelectedSegmentIndex:1];
    }
    
    NSString* score_index = [mydir objectForKey:@"ASAScore"];
    NSInteger index_of_score = [score_index integerValue];
    if(index_of_score < [ASAScoresList count])
        [ASAScore setText:[ASAScoresList objectAtIndex:index_of_score]];
}
/**!
 * For sanitizing empty values
 */
- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Pre Object
	[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
}
/*!
 * Save Data into Model
 */
-(void) SaveDataIntoModel
{
    NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    NSString *reconstructionAge = ReconstructionAge.text;
    NSString *plannedDischargeDate = PlannedDischargeDate.text;
    NSString *actualDischargeDate = ActualDischargeDate.text;
    NSString *bmi = BMI.text;
    NSString *braSize = BraSize.text;
    NSString * comorbid_condition = @"";
    if (ComorbidCondition.selectedSegmentIndex == 0)
    {
        comorbid_condition = @"true"; 
    } else {
        comorbid_condition = @"false";
    }
    NSInteger index_of_object = [ComorbidConditionsList indexOfObject:ComorbidConditionType.text];
    NSString *comorbidConditionType = @"";
    if(index_of_object < [ComorbidConditionsList count])
        comorbidConditionType = [NSString stringWithFormat:@"%d", index_of_object];
    NSString *comorbidConditionOthers = ComorbidConditionTypeOthers.text;
    
    NSInteger asa_score_int = [ASAScoresList indexOfObject:ASAScore.text];
    NSString *asa_score = @"";
    if (asa_score_int < [ASAScoresList count])
        asa_score = [NSString stringWithFormat:@"%d",asa_score_int];
    
    [self SanitizeAndStoreValue:formID forKey:@"formID"];
    [self SanitizeAndStoreValue:reconstructionAge forKey:@"reconstructionAge"];
    [self SanitizeAndStoreValue:plannedDischargeDate forKey:@"plannedDischargeDate"];
    [self SanitizeAndStoreValue:actualDischargeDate forKey:@"actualDischargedDate"];
    [self SanitizeAndStoreValue:bmi forKey:@"BMI"];
    [self SanitizeAndStoreValue:braSize forKey:@"braSize"];
    [self SanitizeAndStoreValue:comorbid_condition forKey:@"comorbidCondition"];
    [self SanitizeAndStoreValue:comorbidConditionType forKey:@"comorbidConditionType"];
    [self SanitizeAndStoreValue:comorbidConditionOthers forKey:@"comorbidConditionOthers"];
    [self SanitizeAndStoreValue:asa_score forKey:@"ASAScore"];
    
    NSLog([[HDMSModelStorage storageObject].Breast_PreObject description]);
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    ReconstructionAge.userInteractionEnabled = TRUE;
    BMI.userInteractionEnabled = TRUE;
    BraSize.userInteractionEnabled = TRUE;
    ComorbidCondition.userInteractionEnabled = TRUE;
    ComorbidConditionTypeOthers.userInteractionEnabled = TRUE;
    SelectDischargeDateButton.hidden = FALSE;
    SelectActualDischargeDateButton.hidden = FALSE;
    SelectASAScoreButton.hidden = FALSE;
}

-(void) EnablePartialControls
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    if ([self isNotEmptyOrNil:ReconstructionAge.text])
        ReconstructionAge.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"plannedDischargeDate"]])
        SelectDischargeDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"actualDischargedDate"]])
        SelectActualDischargeDateButton.hidden = TRUE;
    if([self isNotEmptyOrNil:BMI.text])
        BMI.userInteractionEnabled = FALSE;
    if([self isNotEmptyOrNil:BraSize.text])
        BraSize.userInteractionEnabled = FALSE;
    if ([self isNotEmptyOrNil:[mydir objectForKey:@"comorbidCondition"]])
        ComorbidCondition.userInteractionEnabled = FALSE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"comorbidConditionOthers"]])
        ComorbidConditionTypeOthers.userInteractionEnabled = FALSE;
    if([self isNotEmptyOrNil:[mydir objectForKey:@"ASAScore"]])
        SelectASAScoreButton.hidden = FALSE;
}


- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isPOST]) {
		if ([response statusCode] == 201) {
			[CommonUtils DisplayModalPopupWithMessage:@"Request Succeeded"];
			[CommonUtils StandardDebugLogWithString:[response bodyAsString]];
		} else {
			[CommonUtils DisplayModalPopupWithMessage:[NSString stringWithFormat:@"Request Did not Succeed %@", [response bodyAsString]]];
		}
	} else if ([request isGET]) {
		// Process the json here
		NSString *json = [response bodyAsString];
        
		if ((json == nil) || (json == NULL) || [json isEqualToString:@"null"]) {
			// Nothing its fine
			return;
		} else {
			id <RKParser>	parser		= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
			NSError			*error		= nil;
			NSDictionary	*results	= [parser objectFromString:json error:&error];
            
			[CommonUtils StandardDebugLogWithString:[NSString stringWithFormat:@"Response %@", [response bodyAsString]]];
            
			for (NSString *key in results) {
				NSString *value = [results objectForKey:key];
				[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
			}
		}
        
		if (([response statusCode] == 404) || ([response statusCode] == 400)) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_NEW;
			[self viewDidLoad];
		}
        
		if (([response statusCode] == 201) && [[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"2"]) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_PARTIAL_EDIT;
			[self viewDidLoad];
		} else if (([response statusCode] == 201) && [[HDMSModelStorage storageObject].UserObject.access_level isEqualToString:@"1"]) {
			[HDMSModelStorage storageObject].FormMode = FORM_MODE_EDIT;
			[self viewDidLoad];
		}
	}
}



@end

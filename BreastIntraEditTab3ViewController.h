//
//  BreastIntraEditTab3ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 10/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastIntraEditTab3ViewController : UIViewController <RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *Others;
@property (weak, nonatomic) IBOutlet UITextField *OtherReason;
@property (weak, nonatomic) IBOutlet UITextField *StartTime;
@property (weak, nonatomic) IBOutlet UITextField *EndTime;
@property (weak, nonatomic) IBOutlet UITextField *OtherComments;

- (IBAction)SubmitIntraData:(UIBarButtonItem *)sender;
@end

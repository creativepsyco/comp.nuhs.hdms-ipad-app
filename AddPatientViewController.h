//
//  AddPatientViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 17/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"
@interface AddPatientViewController : UIViewController <RKRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *PatientID;
@property (weak, nonatomic) IBOutlet UITextField *Gender;
@property (weak, nonatomic) IBOutlet UITextField *Ethnicity;
@property (weak, nonatomic) IBOutlet UITextField *Weight;
@property (weak, nonatomic) IBOutlet UITextField *Occupation;
@property (weak, nonatomic) IBOutlet UITextView *Address;
@property (weak, nonatomic) IBOutlet UITextField *MotherID;
@property (weak, nonatomic) IBOutlet UITextField *Name;
@property (weak, nonatomic) IBOutlet UITextField *Birthdate;
@property (weak, nonatomic) IBOutlet UITextField *Height;
@property (weak, nonatomic) IBOutlet UITextField *Smoker;
@property (weak, nonatomic) IBOutlet UITextField *HomeNumber;
@property (weak, nonatomic) IBOutlet UITextField *MobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *OfficeNumber;
@property (weak, nonatomic) IBOutlet UITextField *FatherID;
- (IBAction)AddPatientAction:(id)sender;

- (IBAction)ClearAllFieldsAction:(id)sender;

@end

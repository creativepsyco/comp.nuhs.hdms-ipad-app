//
//  LoginViewControllerViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 29/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "HDMSModelStorage.h"
#import "Constants.h"
#import "Restkit/JSONKit.h"
#import "RestKit/RKRequestSerialization.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField	*userName;
@property (weak, nonatomic) IBOutlet UITextField	*password;
@property (weak, nonatomic) IBOutlet UILabel		*userNameLabel;

@end

@implementation LoginViewController

@synthesize password		= _password;
@synthesize userNameLabel	= _userNameLabel;
@synthesize userName		= _userName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

// All the Login Functions
- (BOOL)doLoginForDEMO
{
	if ([_password.text isEqualToString:@"password"] && [_userName.text isEqualToString:@"user"]) {
		return YES;
	}

	return NO;
}

// Login

- (void)LoginIsSuccessful
{
	[self dismissModalViewControllerAnimated:YES];
}

- (void)LoginIsUnSuccessful
{
	[self DisplayModalPopup];
}

- (void)DisplayModalPopup
{
	UIAlertView *alert = [[UIAlertView alloc]	initWithTitle	:
		@"The username or password entered is wrong"
												message			:
		@"Try it Again "
		delegate
		:self cancelButtonTitle:
		@"Close"
		otherButtonTitles:nil];

	[alert show];
}

- (IBAction)login
{
	// Make a verfication request here
	// [[RKClient sharedClient] get:UserLoginURL delegate:self];
	NSLog(@"I am your RKClient singleton : %@", [RKClient sharedClient]);

	BOOL response = NO;

	if ([HDMSModelStorage storageObject].Config.environment == DEMO) {
		response = [self doLoginForDEMO];

		if (response) {
			[self dismissModalViewControllerAnimated:YES];
		} else {
			[self LoginIsUnSuccessful];
		}
	} else if ([HDMSModelStorage storageObject].Config.environment == TEST) {
		// create a post request
		// Send a POST to a remote resource. The dictionary will be transparently
		// converted into a URL encoded representation and sent along as the request body
		NSString		*userid		= _userName.text;
		NSString		*password	= _password.text;
		NSDictionary	*params		= [NSDictionary dictionaryWithObjectsAndKeys:
			userid, @"userid",
			password, @"password",
			nil];
		// RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
		id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
		NSError			*error	= nil;
		NSString		*json	= [parser stringFromObject:params error:&error];

		[[RKClient sharedClient] post:UserLoginURL params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];

		NSLog(@"JsonString %@", [params JSONString]);
	}
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
	if ([request isGET]) {
		NSLog(@"GET Response for %@", [request URL]);

		if ([response isOK]) {
			NSLog(@"%@", [response body]);
			NSLog(@"%@", [response bodyAsString]);
			NSLog(@"%@", [response isHTML]);
		}
	} else if ([request isPOST]) {
		if ([response statusCode] == 201) {
			// Do the login
			NSString *token = [response bodyAsString];
			NSLog(@"Successfully logged in %@", token);
			[[HDMSModelStorage storageObject] UserObject].access_token = token;
            [[HDMSModelStorage storageObject] UserObject].userid = _userName.text; 
            //[[[[HDMSModelStorage storageObject] OfflineDAO] libraryContent] setValue: token forKey: @"access_token"];
			[self LoginIsSuccessful];
		} else {
			// Login failed
			[self LoginIsUnSuccessful];
			[[HDMSModelStorage storageObject] UserObject].access_token = @"";
            //[[[[HDMSModelStorage storageObject] OfflineDAO] libraryContent] setValue: @"" forKey: @"access_token"];
		}
    [[HDMSModelStorage storageObject] saveOffline ];
	}

	NSLog(@"Status %d and response %@", [response statusCode], [response bodyAsString]);
}

/*
 *   - (void)loadView
 *   {
 *    // If you create your views manually, you MUST override this method and use it to create your views.
 *    // If you use Interface Builder to create your views, then you must NOT override this method.
 *   }*/

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	// Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
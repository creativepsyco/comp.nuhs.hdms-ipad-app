//
//  UpdateUserViewController.m
//  HDMS iPad App
//
//  Created by Xinyu Li on 27/3/12.
//  Copyright (c) 2012 National University of Singapore. All rights reserved.
//

#import "UpdateUserViewController.h"
#import "Constants.h"
#import "RestKit/RKRequestSerialization.h"
#import "HDMSModelStorage.h"

@interface UpdateUserViewController ()

@end

@implementation UpdateUserViewController
@synthesize NameTextField;
@synthesize EmailTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeWithUser:[[HDMSModelStorage storageObject] UserObject]];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setNameTextField:nil];
    [self setEmailTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void) initializeWithUser: (NSDictionary*)  user
{
    //    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    //    NSLog(@"PATIENT  %@", [[HDMSModelStorage storageObject].PatientObject objectForKey:@"identification"] );
    // Make the GET request
    [[RKClient sharedClient] get:[Constants GetUserInfo] delegate:self];
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if ([request isGET]){
        if([response statusCode] == 201){
            //do the settingup 
            NSString * json = [response bodyAsString];
            
            id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
            NSError			*error	= nil;
            NSDictionary * results = [parser objectFromString:json error:&error];
            [self SetUserInformationIntoFields:results];
        }
        else {
            [self ShowPopupDialogBoxWithMessage:@"cant load the user."];
        }
    }
    
    if ([request isPOST]) {
        if([response statusCode] == 201) {
            [self ShowPopupDialogBoxWithMessage:@"Successfully modified User details"];
        } else {
            [self ShowPopupDialogBoxWithMessage:[NSString stringWithFormat:@"Could not modify user info, here is the error %@", [response bodyAsString]] ];
        }
    }
}

-(void) SetUserInformationIntoFields: (NSDictionary *) UserInfo
{
    self.NameTextField.text = [UserInfo objectForKey: @"name"];
    self.EmailTextField.text =[UserInfo objectForKey: @"email"];
}

- (void) ShowPopupDialogBoxWithMessage: (NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self ApplicationTitle] 
                                                    message:message 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (NSString *)ApplicationTitle {
    return @"HDMS iPad Application";
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)UpdateUserAction:(id)sender {
    NSString *token = [[HDMSModelStorage storageObject]UserObject].access_token;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.NameTextField.text, @"name", 
                            self.EmailTextField.text, @"email",
                            token, @"token",
                            nil];
    
    id <RKParser>	parser	= [[RKParserRegistry sharedRegistry] parserForMIMEType:RKMIMETypeJSON];
    NSError			*error	= nil;
    NSString		*json	= [parser stringFromObject:params error:&error];
    
    [[RKClient sharedClient] post:[Constants UpdateUserInfo] params:[RKRequestSerialization serializationWithData:[json dataUsingEncoding:NSUTF8StringEncoding] MIMEType:RKMIMETypeJSON] delegate:self];
}
@end

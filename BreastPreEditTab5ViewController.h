//
//  BreastPreEditTab5ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"

@interface BreastPreEditTab5ViewController : UIViewController
<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    // Arrays to store the stuff
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}

//Common Action
- (IBAction)SelectDateAction:(id)sender;
- (IBAction)SubmitBreastPreData:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *SubmitBreastPreDataButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectMammogramDateButton;
@property (weak, nonatomic) IBOutlet UIButton *SelectXRayDateButton;

// Picker Controls and Arrays

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray* ASAScoresList;
@property (nonatomic, retain) NSArray* ComorbidConditionsList;
@property (nonatomic, retain) UIDatePicker* datePicker;
@property (nonatomic, retain) NSArray* GradesList;


// Tab5 Edit Fields
@property (weak, nonatomic) IBOutlet UITextField *MammogramShowed;
@property (weak, nonatomic) IBOutlet UILabel *MammogramDateDone;
@property (weak, nonatomic) IBOutlet UITextField *XRayShowed;
@property (weak, nonatomic) IBOutlet UILabel *XRayDateDone;

@end

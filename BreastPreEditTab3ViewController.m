//
//  BreastPreEditTab3ViewController.m
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BreastPreEditTab3ViewController.h"

@interface BreastPreEditTab3ViewController ()
{
    int current_item_change_id;
}

@end

@implementation BreastPreEditTab3ViewController
@synthesize LumpPresent;
@synthesize BreastSwelling;
@synthesize AreaThickeningState;
@synthesize RednessState;
@synthesize DimpleBreastState;
@synthesize EnlargedLymphNodeState;
@synthesize MasectomyPosition;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    
	// Logic to handle edit and partial edit stuff
	if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_NEW) 
    {
		// its a new form
		// display as it is
        // Enable all controls
        [self EnableAllControls];
	}
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_EDIT)
    {
		// Store old information first
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
		// now display what the server has already
        // Enable all the controls
        [self LoadDataIntoView];
        [self EnableAllControls];
	} 
    else if ([HDMSModelStorage storageObject].FormMode == FORM_MODE_PARTIAL_EDIT)
    {
		[HDMSModelStorage storageObject].Breast_OldPreObject = [[HDMSModelStorage storageObject].Breast_PreObject copy];
        [self LoadDataIntoView];
        [self EnablePartialControls];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}



- (IBAction)SelectDateAction:(UIButton*)sender {
    current_item_change_id = sender.tag;
    datePicker.hidden = (!datePicker.hidden);
}

/// UI Control Management Region
/*!
 Used to check the sanity of the input
 Returns True if string is not empty
 */
- (BOOL)isNotEmptyOrNil:(NSString *)thing
{
	if ((thing == nil) || [thing isEqualToString:@""] || (thing == NULL)) {
		return FALSE;
	} else {
		return TRUE;
	}
}
- (NSInteger) GetBooleanIndexFromString: (NSString*) theString
{
    if([theString isEqualToString:@"0"] || [theString isEqualToString:@"false"])
        return 0;
    if([theString isEqualToString:@"1"] || [theString isEqualToString:@"true"])
        return 1;
    return 0;
}
/*!
 * Loads the dictionary data into view
 */
-(void) LoadDataIntoView
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    
    NSString* symptomsPresented = [mydir objectForKey:@"symptomsPresented"];
    if([symptomsPresented characterAtIndex:0]=='1')
        [LumpPresent setSelectedSegmentIndex:1];
    else 
        [LumpPresent setSelectedSegmentIndex:0];

    if([symptomsPresented characterAtIndex:1]=='1')
        [BreastSwelling setSelectedSegmentIndex:1];
    else 
        [BreastSwelling setSelectedSegmentIndex:0];

    if([symptomsPresented characterAtIndex:2]=='1')
        [AreaThickeningState setSelectedSegmentIndex:1];
    else 
        [AreaThickeningState setSelectedSegmentIndex:0];

    if([symptomsPresented characterAtIndex:3]=='1')
        [RednessState setSelectedSegmentIndex:1];
    else 
        [RednessState setSelectedSegmentIndex:0];

    if([symptomsPresented characterAtIndex:4]=='1')
        [DimpleBreastState setSelectedSegmentIndex:1];
    else 
        [DimpleBreastState setSelectedSegmentIndex:0];
    
    if([symptomsPresented characterAtIndex:5]=='1')
        [EnlargedLymphNodeState setSelectedSegmentIndex:1];
    else 
        [EnlargedLymphNodeState setSelectedSegmentIndex:0];
    
    if([[mydir objectForKey:@"mastectomyPosition"]isEqualToString:@"1"])
        [MasectomyPosition setSelectedSegmentIndex:1];
    else [MasectomyPosition setSelectedSegmentIndex:0];
    
}
/**!
 * For sanitizing empty values
 */
- (NSString *)sanitizeValue:(NSString *)value
{
	if (value != nil) {
		return value;
	} else {
		return @"";
	}
}

- (void)SanitizeAndStoreValue:(NSString *)value forKey:(NSString *)key
{
	// Only dealing with Breast Pre Object
	[[HDMSModelStorage storageObject].Breast_PreObject setObject:[self sanitizeValue:value] forKey:key];
}
/*!
 * Save Data into Model
 */
-(void) SaveDataIntoModel
{
    NSString *formID = [[HDMSModelStorage storageObject].FormObject objectForKey:@"formID"];
    
    NSMutableString *bitmap = [[NSMutableString alloc]init];
    if([LumpPresent selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    if([BreastSwelling selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    if([AreaThickeningState selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    if([RednessState selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    if([DimpleBreastState selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    if([EnlargedLymphNodeState selectedSegmentIndex]==1)
        [bitmap appendFormat:@"%@",@"1"];
    else [bitmap appendFormat:@"%@",@"0"];
    
    [self SanitizeAndStoreValue:[bitmap copy] forKey:@"symptomsPresented"];
    
    [self SanitizeAndStoreValue:[NSString stringWithFormat:@"%d", MasectomyPosition.selectedSegmentIndex] forKey:@"mastectomyPosition"];
    
    NSLog([[HDMSModelStorage storageObject].Breast_PreObject description]);
}

/*!
 Assumes data is already loaded into the view
 */
-(void) EnableAllControls
{
    LumpPresent.userInteractionEnabled = TRUE;
    BreastSwelling.userInteractionEnabled = TRUE;
    AreaThickeningState.userInteractionEnabled = TRUE;
    RednessState.userInteractionEnabled = TRUE;
    DimpleBreastState.userInteractionEnabled = TRUE;
    EnlargedLymphNodeState.userInteractionEnabled = TRUE;
    MasectomyPosition.userInteractionEnabled = TRUE;
}

-(void) EnablePartialControls
{
    NSMutableDictionary *mydir = [HDMSModelStorage storageObject].Breast_PreObject;
    NSString* symptomsPresented = [mydir objectForKey:@"symptomsPresented"];
    LumpPresent.userInteractionEnabled = FALSE;
    BreastSwelling.userInteractionEnabled = FALSE;
    AreaThickeningState.userInteractionEnabled = FALSE;
    RednessState.userInteractionEnabled = FALSE;
    DimpleBreastState.userInteractionEnabled = FALSE;
    EnlargedLymphNodeState.userInteractionEnabled = FALSE;
    MasectomyPosition.userInteractionEnabled = FALSE;    
}




- (IBAction)NavigateToTab4:(UIBarButtonItem *)sender {
    [self SaveDataIntoModel];
    [self performSegueWithIdentifier:@"FromTab3ToTab4" sender:self];
}
@end

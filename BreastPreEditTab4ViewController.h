//
//  BreastPreEditTab4ViewController.h
//  HDMS iPad App
//
//  Created by Mohit Singh Kanwal on 16/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"
@interface BreastPreEditTab4ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, RKRequestDelegate>
{
    UIPickerView* pickerView;
    UIDatePicker* datePicker;
    
    NSArray* ASAScoresList;
    NSArray* ComorbidConditionsList;
    NSArray* GradesList;
}
//!
//! Common Actions
- (IBAction)SelectDateAction:(id)sender;
//! End Common Actions

// Picker Controls and Arrays

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray* ASAScoresList;
@property (nonatomic, retain) NSArray* ComorbidConditionsList;
@property (nonatomic, retain) UIDatePicker* datePicker;
@property (nonatomic, retain) NSArray* GradesList;

//! Edit Fields

// Tab4 Edit Fields
@property (weak, nonatomic) IBOutlet UILabel *IDCGrade;
@property (weak, nonatomic) IBOutlet UILabel *DCISGrade;
@property (weak, nonatomic) IBOutlet UILabel *ExternalDCISGrade;
@property (weak, nonatomic) IBOutlet UILabel *InfiltrativeDuctalCarcinoma;
@property (weak, nonatomic) IBOutlet UILabel *MucinousCarcinoma;
@property (weak, nonatomic) IBOutlet UITextField *MasectomyOtherReason;
@property (weak, nonatomic) IBOutlet UISegmentedControl *FibroadenomataPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *FatNecrosisPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SacromaPresence;
@property (weak, nonatomic) IBOutlet UISegmentedControl *RecurrentBreastCarcinomaPresence;

//!Actions
- (IBAction)Tab4GradeSelected:(UISegmentedControl *)sender;
- (IBAction)NavigateToTab5:(UIBarButtonItem *)sender;

@end
